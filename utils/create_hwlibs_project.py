#!/usr/bin/python

# 
# Copyright (c) 2017, Intel Corporation <www.intel.com>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# - Redistributions of source code must retain the above copyright
#   notice, this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# - Neither the name of the Altera Corporation nor the
#   names of its contributors may be used to endorse or promote products
#   derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL ALTERA CORPORATION BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys, signal, os, time, re, subprocess, threading, getopt, random
import xml.etree.ElementTree as ET

# global variables
SOCEDS_ROOT = ""
DEVICE = ""
COMPILER = ""
PRINTF_METHOD = ""
PROJECT_NAME = ""
PROJECT_TYPE = "plugin"
MEMORY = ""
PROJECT_TEMPLATE = [];
PLUGIN_ARMCC_CPROJECT_TEMPLATE = [];
MAKEFILE_ARMCC_CPROJECT_TEMPLATE = [];
PLUGIN_GCC_CPROJECT_TEMPLATE = [];
MAKEFILE_GCC_CPROJECT_TEMPLATE = [];
MAKEFILE_GCC_TEMPLATE = [];
MAKEFILE_ARMCC_TEMPLATE = [];
MAIN_C_TEMPLATE_UART = [];
MAIN_C_TEMPLATE_SEMIHOSTING = [];
CV_DEBUG_CONFIGURATION_LAUNCH_TEMPLATE = [];
AV_DEBUG_CONFIGURATION_LAUNCH_TEMPLATE = [];
A10_DEBUG_CONFIGURATION_LAUNCH_TEMPLATE = [];
BOOTLOADER_SCRIPT = "";
FPGA_CONFIG_FILE = "";
FPGA_CONFIG_LAUNCH_TEMPLATE = [];
CV_PRELOADER_LAUNCH_TEMPLATE = [];
AV_PRELOADER_LAUNCH_TEMPLATE = [];
A10_UBOOT_LAUNCH_TEMPLATE = [];
SVD_FOLDER = "";
CREATE_HEADERS_LAUNCH_TEMPLATE = "";
SOPCINFO_FILE = "";

def run_command(command):
	print "\033[1;32m Running: " + command + "\033[0m"
	os.system(command)

def print_error(message):
	print "\033[1;31m" + message + "\033[0m";

def create_main_c_file(filename):
	print "Creating main source file: " + filename
	
	# read the template
	if PRINTF_METHOD == "uart":
		lines = MAIN_C_TEMPLATE_UART;
	
	else:
		lines = MAIN_C_TEMPLATE_SEMIHOSTING;

	
	# take care of semihosting
	new_lines = [];
	for line in lines:
		if line == "my_semihosting":
			if COMPILER == "armcc":
				new_lines.append("");
				new_lines.append("int __auto_semihosting;");
			line = "";

		new_lines.append(line);	
	
	# create the file
	file = open(filename, "w")
	for line in new_lines:
		file.write("%s\n" % (line))
	file.close()
	
def create_debug_launcher_file(filename):

	print "Creating debug configuration file: " + filename

	if DEVICE == "cv":
		lines = CV_DEBUG_CONFIGURATION_LAUNCH_TEMPLATE;
	elif DEVICE == "av":
		lines = AV_DEBUG_CONFIGURATION_LAUNCH_TEMPLATE;
	else:
		lines = A10_DEBUG_CONFIGURATION_LAUNCH_TEMPLATE;

	my_svd_folder = SVD_FOLDER;
	if my_svd_folder == "default":
		if DEVICE == "cv":
			my_svd_folder = "${env_var:SOCEDS_DEST_ROOT}/examples/hardware/cv_soc_devkit_ghrd/soc_system/synthesis";
		elif DEVICE == "av":
			my_svd_folder = "${env_var:SOCEDS_DEST_ROOT}/examples/hardware/av_soc_devkit_ghrd/ghrd_5astfd5k3/synthesis"
		else:
			my_svd_folder = "${env_var:SOCEDS_DEST_ROOT}/examples/hardware/a10_soc_devkit_ghrd/ghrd_10as066n2"

	if PROJECT_TYPE == "plugin":
		my_project_name = "/" + PROJECT_NAME + "/Debug/" + PROJECT_NAME + ".axf";
	else:
		my_project_name = "/" + PROJECT_NAME + "/" + PROJECT_NAME + ".axf";

	new_lines = [];
	for line in lines:
		line = line.replace("my_project_name.axf", my_project_name);
		line = line.replace("my_svd_folder", my_svd_folder);

		if "my_ice_debug_resources_count" in line:
			if my_svd_folder <> "":
				new_lines.append("<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.1.TYPE\" value=\"SFR_SEARCH_DIRS\"/>");
				new_lines.append("<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.1.VALUE\" value=\"" + my_svd_folder + "\"/>");
				line = line.replace("my_ice_debug_resources_count", "2");
			else:
				line = line.replace("my_ice_debug_resources_count", "1");
		new_lines.append(line);
	
	# create the file
	file = open(filename, "w")
	for line in new_lines:
		file.write("%s\n" % (line))
	file.close()
	
	

def create_project_file(filename):
	print "Creating .project file: " + filename
	
	#read the template
	lines = PROJECT_TEMPLATE;
	
	# fill in the project name
	new_lines = [];
	for line in lines:
		line = line.replace("my_project_name", PROJECT_NAME);
		new_lines.append(line);	
	
	# create the file
	file = open(filename, "w")
	for line in new_lines:
		file.write("%s\n" % (line))
	file.close()
	
def get_linker_script_name():

	linker_script = "";
	if COMPILER == "gcc":
		if DEVICE == "a10":
			if PRINTF_METHOD == "uart":
				linker_script = "arria10-dk-ram.ld";
			else:
				linker_script = "arria10-dk-ram-hosted.ld";
		else:
			if PRINTF_METHOD == "uart":
				linker_script = "cycloneV-dk-ram.ld";
			else:
				linker_script = "cycloneV-dk-ram-hosted.ld";
		if(MEMORY == "ocram"):
			linker_script = linker_script.replace("-ram", "-oc-ram");
	else:
		linker_script =  DEVICE + "-" + MEMORY + ".scat";
	
	return linker_script;
	
def	create_armcc_linker_script(filename):

	print "Creating ARMCC linker script file: " + filename

	# determine parameters
	if MEMORY == "sdram":
		mem_start = "0x100000";      # 1M
		mem_size = "0x3FF00000";     # 1023M
		mem_end = "0x40000000";
	else:
		if DEVICE == "a10":		
			mem_start = "0xFFE00000";
			mem_size = "0x40000";    # 256K
			mem_end = "0xFFE40000";
		else:
			mem_start = "0xFFFF0000";
			mem_size = "0x10000";    # 64K
			mem_end = "0xFFFFFFF0";  # we actually loose 16 bytes here, since linker does not accept 4G

	# create file

	file = open(filename, "w")	
	file.write("%s %s %s\n" % (MEMORY.upper(), mem_start, mem_size));
	file.write("{\n");
	file.write("    VECTORS +0\n");  
	file.write("    {\n");
	file.write("        *(VECTORS, +FIRST)\n");
	file.write("    }\n");
	file.write("\n");
	file.write("    APP_CODE +0\n");  
	file.write("    {\n");
	file.write("        *(+RO, +RW, +ZI)\n");
	file.write("    }\n");
	file.write("\n");
	file.write("    ARM_LIB_STACKHEAP +0 EMPTY (%s - ImageLimit(APP_CODE))\n" % (mem_end));
	file.write("    {}\n");
	file.write("\n");
	file.write("}\n");
	file.close()


def create_cproject_file(filename):
	
	print "Creating .cproject file: " + filename
	
	# read template
	if PROJECT_TYPE == "plugin" :
		if COMPILER == "gcc":
			lines = PLUGIN_GCC_CPROJECT_TEMPLATE;
		else:
			lines = PLUGIN_ARMCC_CPROJECT_TEMPLATE;
	else:
		if COMPILER == "gcc":
			lines = MAKEFILE_GCC_CPROJECT_TEMPLATE;
		else:
			lines = MAKEFILE_ARMCC_CPROJECT_TEMPLATE;

	# determine device path
	if (DEVICE == "a10"):
		soc_device = "soc_a10"
	else:
		soc_device = "soc_cv_av";
		
	# read the hashes
	hashes = {}
	for line in lines:
		for hash in re.findall("[0-9]{9,10}", line):
			hashes[hash] = "";
			
	# create new hashes
	reverse_hashes = {}
	for key in hashes.keys():
		new_hash = "";
		while new_hash == "":
			new_hash = "".join(random.choice('0123456789') for i in range(len(key)));
			if(new_hash in hashes.keys()) or (new_hash in reverse_hashes.keys()):
				new_hash = "";
		hashes[key] =  new_hash;
		reverse_hashes[new_hash] = key;				
			
	# print the results			
	#for key in hashes.keys():
	#	print key + " > " + hashes[key];
		
	# process the lines 
	new_lines = [];
	for line in lines:
		# replace the hash keys
		for key, value in hashes.iteritems():	
			line = line.replace(key, value); 
			
		# deal with the macro definitions
		if "value=\"soc_device\"" in line:			
			# add the macro definitions for the printf
			if PRINTF_METHOD == "uart":
				new_lines.append("									<listOptionValue builtIn=\"false\" value=\"PRINTF_UART\"/>");
			else:
				new_lines.append("									<listOptionValue builtIn=\"false\" value=\"PRINTF_HOST\"/>");
                        # Add STDC_HEADERS for newer hwlibs including safeclib
			new_lines.append("									<listOptionValue builtIn=\"false\" value=\"STDC_HEADERS\"/>");
			# fill in the corrct device
			line = line.replace("soc_device", soc_device); 

		# deal with the linker script
		linker_script = get_linker_script_name()
		line = line.replace("my_linker_script", linker_script);		

		if PRINTF_METHOD == "uart":
			line = line.replace("my_gcc_enable_semihosting", "");
		else:
			line = line.replace("my_gcc_enable_semihosting", " -Wl,--defsym=__auto_semihosting=1");
		
		# add the new line
		new_lines.append(line);
		
	file = open(filename, "w")
	for line in new_lines:
		file.write("%s\n" % (line))
	file.close()
	 
		
def bring_project_files():
	
	# determine device path
	if (DEVICE == "a10"):
		devpath = "soc_a10"
	else:
		devpath = "soc_cv_av";

	# copy the files from soceds
	run_command("rm -rf " + PROJECT_NAME + "/hwlibs");
	run_command("mkdir " + PROJECT_NAME + "/hwlibs");
	run_command("mkdir " + PROJECT_NAME + "/hwlibs/include");
	run_command("mkdir " + PROJECT_NAME + "/hwlibs/src");
	run_command("cp " + SOCEDS_ROOT + "/ip/altera/hps/altera_hps/hwlib/include/*.h " + PROJECT_NAME + "/hwlibs/include/");
	run_command("cp -r " + SOCEDS_ROOT + "/ip/altera/hps/altera_hps/hwlib/include/" + devpath + "/* " + PROJECT_NAME + "/hwlibs/include/");
	run_command("cp " + SOCEDS_ROOT + "/ip/altera/hps/altera_hps/hwlib/src/hwmgr/* " + PROJECT_NAME + "/hwlibs/src/");
	run_command("cp " + SOCEDS_ROOT + "/ip/altera/hps/altera_hps/hwlib/src/hwmgr/" + devpath + "/* " + PROJECT_NAME + "/hwlibs/src/");
        # Jeff : Adding something to copy safeclib
	run_command("cp -a " + SOCEDS_ROOT + "/ip/altera/hps/altera_hps/hwlib/include/" + "safeclib " + PROJECT_NAME + "/hwlibs/include/");
	run_command("cp " + SOCEDS_ROOT + "/ip/altera/hps/altera_hps/hwlib/src/" + "safeclib" + "/* " + PROJECT_NAME + "/hwlibs/src/");

	# remove the not needed files
	run_command("rm -f " + PROJECT_NAME + "/hwlibs/src/alt_interrupt_armclang.s");
	run_command("rm -f " + PROJECT_NAME + "/hwlibs/src/alt_clock_manager_init.c");
	if COMPILER == "gcc":
		run_command("rm -f " + PROJECT_NAME + "/hwlibs/src/alt_interrupt.s");
		run_command("rm -f " + PROJECT_NAME + "/hwlibs/src/alt_interrupt_armcc.s");
		
	if (DEVICE == "cv") or (DEVICE == "av"):
		if COMPILER == "gcc":
			run_command("rm -f " + PROJECT_NAME + "/hwlibs/src/alt_bridge_f2s_armcc.s");
		else:
			run_command("rm -f " + PROJECT_NAME + "/hwlibs/src/alt_bridge_f2s_gnu.s");

	# deal with UART printf
	if PRINTF_METHOD == "uart":
		run_command("cp " + SOCEDS_ROOT + "/ip/altera/hps/altera_hps/hwlib/src/utils/alt_printf.c " + PROJECT_NAME + "/hwlibs/src/");
		run_command("cp " + SOCEDS_ROOT + "/ip/altera/hps/altera_hps/hwlib/src/utils/alt_p2uart.c " + PROJECT_NAME + "/hwlibs/src/");
	else:
		run_command("rm -f " + PROJECT_NAME + "/hwlibs/src/alt_16550_uart.c");

	# deal with the linker script
	linker_script = get_linker_script_name()
	if COMPILER == "gcc":
		run_command("cp -f " + SOCEDS_ROOT + "/host_tools/mentor/gnu/arm/baremetal/arm-altera-eabi/lib/" + linker_script + " " + PROJECT_NAME + "/");
	else:
		create_armcc_linker_script(PROJECT_NAME + "/" + linker_script);
		
def create_bootloader_launcher():

	# determine launcher name and template
	if DEVICE == "cv":
		#filename = PROJECT_NAME + "/cv-preloader.launch";
		filename = PROJECT_NAME + "/" + PROJECT_NAME + "-Run-Bootloader.launch";
		lines = CV_PRELOADER_LAUNCH_TEMPLATE;
	elif DEVICE == "av":
		#filename = PROJECT_NAME + "/av-preloader.launch";
		filename = PROJECT_NAME + "/" + PROJECT_NAME + "-Run-Bootloader.launch";
		lines = AV_PRELOADER_LAUNCH_TEMPLATE;
	else:
		#filename = PROJECT_NAME + "/a10-uboot.launch";
		filename = PROJECT_NAME + "/" + PROJECT_NAME + "-Run-Bootloader.launch";
		lines = A10_UBOOT_LAUNCH_TEMPLATE;

	# determine bootloader script
	my_bootloader_script = BOOTLOADER_SCRIPT;
	if my_bootloader_script == "default":
		if DEVICE == "cv":
			my_bootloader_script = "${env_var:SOCEDS_DEST_ROOT}/examples/hardware/cv_soc_devkit_ghrd/software/preloader/preloader.ds"
		elif DEVICE == "av":
			my_bootloader_script = "${env_var:SOCEDS_DEST_ROOT}/examples/hardware/av_soc_devkit_ghrd/software/preloader/preloader.ds"
		else:
			my_bootloader_script = "${env_var:SOCEDS_DEST_ROOT}/examples/hardware/a10_soc_devkit_ghrd/software/bootloader_ext_cfg/uboot.ds"

	print "Creating launcher file: " + filename
			
	# process the lines 
	new_lines = [];
	for line in lines:
		line = line.replace("my_bootloader_script", my_bootloader_script);
		new_lines.append(line);

	# create the output
	file = open(filename, "w")
	for line in new_lines:
		file.write("%s\n" % (line))
	file.close()

def create_fpga_config_launcher():

	#filename = PROJECT_NAME + "/" + DEVICE + "-fpga-config.launch";
	filename = PROJECT_NAME + "/" + PROJECT_NAME + "-Configure-FPGA.launch";

	print "Creating FPGA configuration launcher file: " + filename

	my_sof_path = FPGA_CONFIG_FILE;
	if my_sof_path == "default":
		if DEVICE == "cv":
			my_sof_path = "${env_var:SOCEDS_DEST_ROOT}/examples/hardware/cv_soc_devkit_ghrd/output_files/soc_system.sof@2";
		elif DEVICE == "av":
			my_sof_path = "${env_var:SOCEDS_DEST_ROOT}/examples/hardware/av_soc_devkit_ghrd/output_files/ghrd_5astfd5k3.sof@2";
		else:
			my_sof_path = "${env_var:SOCEDS_DEST_ROOT}/examples/hardware/a10_soc_devkit_ghrd/output_files/ghrd_10as066n2.sof";

	# retrieve the template
	lines = FPGA_CONFIG_LAUNCH_TEMPLATE;
		
	# process the lines 
	new_lines = [];
	for line in lines:
		line = line.replace("my_sof_path", my_sof_path);
		new_lines.append(line);

	# create the output
	file = open(filename, "w")
	for line in new_lines:
		file.write("%s\n" % (line))
	file.close()


def create_system_h_file():

	# determine the sopcinfo file to use
	my_sopcinfo_file = SOPCINFO_FILE;
	crt_sopcinfo_file = my_sopcinfo_file;
	if my_sopcinfo_file == "default":
		if DEVICE == "cv":
			my_sopcinfo_file = "${env_var:SOCEDS_DEST_ROOT}/examples/hardware/cv_soc_devkit_ghrd/soc_system.sopcinfo";
		elif DEVICE == "av":
			my_sopcinfo_file = "${env_var:SOCEDS_DEST_ROOT}/examples/hardware/av_soc_devkit_ghrd/ghrd_5astfd5k3.sopcinfo";
		else:
			my_sopcinfo_file = "${env_var:SOCEDS_DEST_ROOT}/examples/hardware/a10_soc_devkit_ghrd/ghrd_10as066n2.sopcinfo";

	# expand the actual path of the sopcinfo file, to generate the initial system.h file
	crt_sopcinfo_file = my_sopcinfo_file.replace("${env_var:SOCEDS_DEST_ROOT}", SOCEDS_ROOT);
	if not os.path.isfile(crt_sopcinfo_file):
		print_error("Erorr: could not locate file: " + crt_sopcinfo_file);
		return;

	# find the A9 master in the sopcinfo file
	master = "";
	for module in ET.parse(crt_sopcinfo_file).getroot().findall("module"):
		if "arm_a9" == module.attrib["kind"]:
			master = module.attrib["name"];
			break;
	if "" == master:
		print_error("Error: was not able to find the A9 master in the sopcinfo file");
		return;

	# generate the initial system.h file
	run_command("sopc-create-header-files " + crt_sopcinfo_file + " --module " + master + " --single " +  PROJECT_NAME + "/system.h");

	# create a launcher, so that the file can be re-created as needed
	filename = PROJECT_NAME + "/" + PROJECT_NAME + "-Create-SystemH.launch";

	print "Creating FPGA configuration launcher file: " + filename

	# retrieve the template
	lines = CREATE_HEADERS_LAUNCH_TEMPLATE;
		
	# process the lines 
	new_lines = [];
	for line in lines:
		line = line.replace("my_sopcinfo", my_sopcinfo_file);
		line = line.replace("my_module", master);
		line = line.replace("my_system_file","${workspace_loc}/" +  PROJECT_NAME + "/system.h");
		new_lines.append(line);

	# create the output
	file = open(filename, "w")
	for line in new_lines:
		file.write("%s\n" % (line))
	file.close()

def get_hwlibs_src():
	list = "";
	for filename in os.listdir(PROJECT_NAME + "/hwlibs/src"):
		list = list + " " + filename;
	return list;

def create_makefile():

	filename = PROJECT_NAME + "/Makefile";	
	
	print "Creating Makefile " + filename;
	
	# read template
	if COMPILER == "gcc":
		lines = MAKEFILE_GCC_TEMPLATE;
	else:
		lines = MAKEFILE_ARMCC_TEMPLATE;

	# determine device path
	if (DEVICE == "a10"):
		soc_device = "soc_a10"
	else:
		soc_device = "soc_cv_av";
		
	# determine linker script
	my_linker_script = get_linker_script_name()
	
	# determine printf method
	if PRINTF_METHOD == "uart":			
		my_printf_option = "PRINTF_UART"
		my_gcc_enable_semihosting = ""
	else:
		my_printf_option = "PRINTF_HOST"
		my_gcc_enable_semihosting = " -Wl,--defsym=__auto_semihosting=1"

	# determine list of hwlibs source code
	my_hwlibs_src = get_hwlibs_src();

	# process the template
	new_lines = [];
	for line in lines:

		line = line.replace("my_project_name", PROJECT_NAME);
		line = line.replace("my_printf_option", my_printf_option);
		line = line.replace("my_gcc_enable_semihosting", my_gcc_enable_semihosting);  
		line = line.replace("soc_device", soc_device); 
		line = line.replace("my_linker_script", my_linker_script);		
		line = line.replace("my_hwlibs_src", my_hwlibs_src);
	
		new_lines.append(line);


	file = open(filename, "w")
	for line in new_lines:
		file.write("%s\n" % (line))
	file.close()

	return;


def initialize_templates():	

	global PROJECT_TEMPLATE;
	global PLUGIN_ARMCC_CPROJECT_TEMPLATE;
	global MAKEFILE_ARMCC_CPROJECT_TEMPLATE;
	global PLUGIN_GCC_CPROJECT_TEMPLATE;
	global MAKEFILE_GCC_CPROJECT_TEMPLATE;
	global MAKEFILE_GCC_TEMPLATE;
	global MAKEFILE_ARMCC_TEMPLATE;
	global MAIN_C_TEMPLATE_UART;
	global MAIN_C_TEMPLATE_SEMIHOSTING;
	global CV_DEBUG_CONFIGURATION_LAUNCH_TEMPLATE;
	global AV_DEBUG_CONFIGURATION_LAUNCH_TEMPLATE;
	global A10_DEBUG_CONFIGURATION_LAUNCH_TEMPLATE;
	global FPGA_CONFIG_LAUNCH_TEMPLATE;
	global CV_PRELOADER_LAUNCH_TEMPLATE;
	global AV_PRELOADER_LAUNCH_TEMPLATE;
	global A10_UBOOT_LAUNCH_TEMPLATE;
	global CREATE_HEADERS_LAUNCH_TEMPLATE;

	MAIN_C_TEMPLATE_SEMIHOSTING = [
		"#include <stdio.h>",
		"my_semihosting",
		"int main(void)",
		"{",
		"\tprintf(\"Hello World!\\n\");",
		"\treturn 0;",
		"}"	
	];

	
	MAIN_C_TEMPLATE_UART = [
		"#include \"alt_printf.h\"",		
		"",
		"int main(void)",
		"{",
		"\tprintf(\"Hello World!\\n\");",
		"\treturn 0;",
		"}"	
	];
	
	PROJECT_TEMPLATE = [
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
		"<projectDescription>",
		"   <name>my_project_name</name>",
		"   <comment></comment>",
		"   <projects>",
		"   </projects>",
		"   <buildSpec>",
		"           <buildCommand>",
		"                   <name>org.eclipse.cdt.managedbuilder.core.genmakebuilder</name>",
		"                   <triggers>clean,full,incremental,</triggers>",
		"                   <arguments>",
		"                   </arguments>",
		"           </buildCommand>",
		"           <buildCommand>",
		"                   <name>org.eclipse.cdt.managedbuilder.core.ScannerConfigBuilder</name>",
		"                   <triggers>full,incremental,</triggers>",
		"                   <arguments>",
		"                   </arguments>",
		"           </buildCommand>",
		"   </buildSpec>",
		"   <natures>",
		"           <nature>org.eclipse.cdt.core.cnature</nature>",
		"           <nature>org.eclipse.cdt.managedbuilder.core.managedBuildNature</nature>",
		"           <nature>org.eclipse.cdt.managedbuilder.core.ScannerConfigNature</nature>",
		"   </natures>",
		"</projectDescription>"
	];

	PLUGIN_ARMCC_CPROJECT_TEMPLATE = [
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>",
		"<?fileVersion 4.0.0?><cproject storage_type_id=\"org.eclipse.cdt.core.XmlProjectDescriptionStorage\">",
		"	<storageModule moduleId=\"org.eclipse.cdt.core.settings\">",
		"		<cconfiguration id=\"com.arm.eclipse.build.config.baremetal.exe.debug.1060154171\">",
		"			<storageModule buildSystemId=\"org.eclipse.cdt.managedbuilder.core.configurationDataProvider\" id=\"com.arm.eclipse.build.config.baremetal.exe.debug.1060154171\" moduleId=\"org.eclipse.cdt.core.settings\" name=\"Debug\">",
		"				<externalSettings/>",
		"				<extensions>",
		"					<extension id=\"org.eclipse.cdt.core.GmakeErrorParser\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"com.arm.eclipse.builder.armcc.error\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"org.eclipse.cdt.core.CWDLocator\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"				</extensions>",
		"			</storageModule>",
		"			<storageModule moduleId=\"cdtBuildSystem\" version=\"4.0.0\">",
		"				<configuration artifactExtension=\"axf\" artifactName=\"${ProjName}\" buildArtefactType=\"org.eclipse.cdt.build.core.buildArtefactType.exe\" buildProperties=\"org.eclipse.cdt.build.core.buildArtefactType=org.eclipse.cdt.build.core.buildArtefactType.exe,org.eclipse.cdt.build.core.buildType=org.eclipse.cdt.build.core.buildType.debug\" cleanCommand=\"clean\" description=\"\" id=\"com.arm.eclipse.build.config.baremetal.exe.debug.1060154171\" name=\"Debug\" parent=\"com.arm.eclipse.build.config.baremetal.exe.debug\">",
		"					<folderInfo id=\"com.arm.eclipse.build.config.baremetal.exe.debug.1060154171.\" name=\"/\" resourcePath=\"\">",
		"						<toolChain id=\"com.arm.toolchain.baremetal.exe.debug.73689006\" name=\"ARM Compiler 5 (DS-5 built-in)\" superClass=\"com.arm.toolchain.baremetal.exe.debug\">",
		"							<option id=\"com.arm.toolchain.ac5.option.target.cpu_fpu.412762670\" superClass=\"com.arm.toolchain.ac5.option.target.cpu_fpu\" value=\"Cortex-A9.VFPv3_FP16.Neon\" valueType=\"string\"/>",
		"							<option id=\"com.arm.toolchain.ac5.option.fppcs.368662108\" superClass=\"com.arm.toolchain.ac5.option.fppcs\" value=\"com.arm.tool.c.compiler.option.fppcs.hard\" valueType=\"enumerated\"/>",
		"							<option id=\"com.arm.toolchain.ac5.option.inst.1544158098\" superClass=\"com.arm.toolchain.ac5.option.inst\" value=\"com.arm.tool.c.compiler.option.inst.arm\" valueType=\"enumerated\"/>",
		"							<targetPlatform id=\"com.arm.eclipse.build.config.baremetal.exe.debug.1060154171..1562422371\" name=\"\"/>",
		"							<builder buildPath=\"${workspace_loc:/ArmccHwlibsProject3}/Debug\" id=\"com.arm.toolchain.baremetal.builder.2069200803\" keepEnvironmentInBuildfile=\"false\" managedBuildOn=\"true\" name=\"Gnu Make Builder\" superClass=\"com.arm.toolchain.baremetal.builder\"/>",
		"							<tool id=\"com.arm.tool.c.compiler.baremetal.exe.debug.493600168\" name=\"ARM C Compiler 5\" superClass=\"com.arm.tool.c.compiler.baremetal.exe.debug\">",
		"								<option defaultValue=\"com.arm.tool.c.compiler.option.optlevel.min\" id=\"com.arm.tool.c.compiler.baremetal.exe.debug.base.option.opt.943895395\" name=\"Optimization level\" superClass=\"com.arm.tool.c.compiler.baremetal.exe.debug.base.option.opt\" useByScannerDiscovery=\"true\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.tool.c.compiler.option.targetcpu.33753150\" superClass=\"com.arm.tool.c.compiler.option.targetcpu\" value=\"Cortex-A9\" valueType=\"string\"/>",
		"								<option id=\"com.arm.tool.c.compiler.option.fppcs.379954248\" superClass=\"com.arm.tool.c.compiler.option.fppcs\" value=\"com.arm.tool.c.compiler.option.fppcs.hard\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.tool.c.compiler.option.inst.1933067178\" superClass=\"com.arm.tool.c.compiler.option.inst\" value=\"com.arm.tool.c.compiler.option.inst.arm\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.tool.c.compiler.option.incpath.1901860699\" superClass=\"com.arm.tool.c.compiler.option.incpath\" valueType=\"includePath\">",
		"									<listOptionValue builtIn=\"false\" value=\"&quot;${workspace_loc:/${ProjName}}&quot;\"/>",
		"									<listOptionValue builtIn=\"false\" value=\"&quot;${workspace_loc:/${ProjName}}/src&quot;\"/>",
		"									<listOptionValue builtIn=\"false\" value=\"&quot;${workspace_loc:/${ProjName}/hwlibs/include}&quot;\"/>",
		"								</option>",
		"								<option id=\"com.arm.tool.c.compiler.option.defmac.2055642540\" superClass=\"com.arm.tool.c.compiler.option.defmac\" valueType=\"definedSymbols\">",
		"									<listOptionValue builtIn=\"false\" value=\"soc_device\"/>",
		"								</option>",
		"								<option id=\"com.arm.tool.c.compile.option.lang.374098639\" superClass=\"com.arm.tool.c.compile.option.lang\" value=\"com.arm.tool.c.compile.option.lang.c99\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.tool.c.compiler.option.strict.1177248911\" name=\"Strict language conformance\" superClass=\"com.arm.tool.c.compiler.option.strict\" useByScannerDiscovery=\"true\" value=\"com.arm.tool.c.compiler.option.strict.error\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.tool.c.compiler.option.warnaserr.1694874995\" name=\"Warnings are errors (--diag_error=warning)\" superClass=\"com.arm.tool.c.compiler.option.warnaserr\" useByScannerDiscovery=\"false\" value=\"true\" valueType=\"boolean\"/>",
		"								<inputType id=\"com.arm.tool.c.compiler.input.1853896753\" superClass=\"com.arm.tool.c.compiler.input\"/>",
		"								<inputType id=\"com.arm.tool.cpp.compiler.input.1816225837\" superClass=\"com.arm.tool.cpp.compiler.input\"/>",
		"							</tool>",
		"							<tool id=\"com.arm.tool.cpp.compiler.baremetal.exe.debug.2057651210\" name=\"ARM C++ Compiler 5\" superClass=\"com.arm.tool.cpp.compiler.baremetal.exe.debug\">",
		"								<option defaultValue=\"com.arm.tool.c.compiler.option.optlevel.min\" id=\"com.arm.tool.cpp.compiler.baremetal.exe.debug.base.option.opt.1613299321\" name=\"Optimization level\" superClass=\"com.arm.tool.cpp.compiler.baremetal.exe.debug.base.option.opt\" valueType=\"enumerated\"/>",
		"							</tool>",
		"							<tool id=\"com.arm.tool.assembler.2005134512\" name=\"ARM Assembler 5\" superClass=\"com.arm.tool.assembler\">",
		"								<option id=\"com.arm.tool.assembler.option.cpu.2035997277\" superClass=\"com.arm.tool.assembler.option.cpu\" value=\"Cortex-A9\" valueType=\"string\"/>",
		"								<option id=\"com.arm.tool.assembler.option.fppcs.2138481969\" superClass=\"com.arm.tool.assembler.option.fppcs\" value=\"com.arm.tool.c.compiler.option.fppcs.hard\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.tool.assembler.option.inst.1229108397\" superClass=\"com.arm.tool.assembler.option.inst\" value=\"com.arm.tool.c.compiler.option.inst.arm\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.tool.assembler.option.err.1432834369\" name=\"Error severity (--diag_error)\" superClass=\"com.arm.tool.assembler.option.err\" value=\"warning\" valueType=\"string\"/>",
		"								<inputType id=\"com.arm.tool.assembler.input.698095467\" superClass=\"com.arm.tool.assembler.input\"/>",
		"							</tool>",
		"							<tool id=\"com.arm.tool.c.linker.43697227\" name=\"ARM Linker 5\" superClass=\"com.arm.tool.c.linker\">",
		"								<option id=\"com.arm.tool.c.linker.option.cpu.770499531\" superClass=\"com.arm.tool.c.linker.option.cpu\" value=\"Cortex-A9\" valueType=\"string\"/>",
		"								<option id=\"com.arm.tool.c.linker.option.scatter.602869167\" superClass=\"com.arm.tool.c.linker.option.scatter\" value=\"${workspace_loc:/${ProjName}}/my_linker_script\" valueType=\"string\"/>",
		"								<option id=\"com.arm.tool.c.linker.option.flags.1209344248\" name=\"Other flags\" superClass=\"com.arm.tool.c.linker.option.flags\" valueType=\"stringList\">",
		"									<listOptionValue builtIn=\"false\" value=\"--entry=alt_interrupt_vector\"/>",
		"								</option>",
		"								<option id=\"com.arm.tool.c.linker.option.err.1426381517\" name=\"Error severity (--diag_error)\" superClass=\"com.arm.tool.c.linker.option.err\" value=\"warning\" valueType=\"string\"/>",
		"							</tool>",
		"							<tool id=\"com.arm.tool.librarian.651911886\" name=\"ARM Librarian 5\" superClass=\"com.arm.tool.librarian\"/>",
		"						</toolChain>",
		"					</folderInfo>",
		"				</configuration>",
		"			</storageModule>",
		"			<storageModule moduleId=\"org.eclipse.cdt.core.externalSettings\"/>",
		"		</cconfiguration>",
		"		<cconfiguration id=\"com.arm.eclipse.build.config.baremetal.exe.release.31761846\">",
		"			<storageModule buildSystemId=\"org.eclipse.cdt.managedbuilder.core.configurationDataProvider\" id=\"com.arm.eclipse.build.config.baremetal.exe.release.31761846\" moduleId=\"org.eclipse.cdt.core.settings\" name=\"Release\">",
		"				<externalSettings/>",
		"				<extensions>",
		"					<extension id=\"org.eclipse.cdt.core.GmakeErrorParser\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"com.arm.eclipse.builder.armcc.error\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"org.eclipse.cdt.core.CWDLocator\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"				</extensions>",
		"			</storageModule>",
		"			<storageModule moduleId=\"cdtBuildSystem\" version=\"4.0.0\">",
		"				<configuration artifactExtension=\"axf\" artifactName=\"${ProjName}\" buildArtefactType=\"org.eclipse.cdt.build.core.buildArtefactType.exe\" buildProperties=\"org.eclipse.cdt.build.core.buildArtefactType=org.eclipse.cdt.build.core.buildArtefactType.exe,org.eclipse.cdt.build.core.buildType=org.eclipse.cdt.build.core.buildType.release\" cleanCommand=\"clean\" description=\"\" id=\"com.arm.eclipse.build.config.baremetal.exe.release.31761846\" name=\"Release\" parent=\"com.arm.eclipse.build.config.baremetal.exe.release\">",
		"					<folderInfo id=\"com.arm.eclipse.build.config.baremetal.exe.release.31761846.\" name=\"/\" resourcePath=\"\">",
		"						<toolChain id=\"com.arm.toolchain.baremetal.exe.release.314170352\" name=\"ARM Compiler 5 (DS-5 built-in)\" superClass=\"com.arm.toolchain.baremetal.exe.release\">",
		"							<option id=\"com.arm.toolchain.ac5.option.target.cpu_fpu.974090938\" superClass=\"com.arm.toolchain.ac5.option.target.cpu_fpu\" value=\"Cortex-A9.VFPv3_FP16.Neon\" valueType=\"string\"/>",
		"							<option id=\"com.arm.toolchain.ac5.option.fppcs.1332925312\" superClass=\"com.arm.toolchain.ac5.option.fppcs\" value=\"com.arm.tool.c.compiler.option.fppcs.hard\" valueType=\"enumerated\"/>",
		"							<option id=\"com.arm.toolchain.ac5.option.inst.1481420609\" superClass=\"com.arm.toolchain.ac5.option.inst\" value=\"com.arm.tool.c.compiler.option.inst.arm\" valueType=\"enumerated\"/>",
		"							<targetPlatform id=\"com.arm.eclipse.build.config.baremetal.exe.release.31761846..1599587734\" name=\"\"/>",
		"							<builder buildPath=\"${workspace_loc:/ArmccHwlibsProject3}/Release\" id=\"com.arm.toolchain.baremetal.builder.1583767051\" keepEnvironmentInBuildfile=\"false\" managedBuildOn=\"true\" name=\"Gnu Make Builder\" superClass=\"com.arm.toolchain.baremetal.builder\"/>",
		"							<tool id=\"com.arm.tool.c.compiler.baremetal.exe.release.955750046\" name=\"ARM C Compiler 5\" superClass=\"com.arm.tool.c.compiler.baremetal.exe.release\">",
		"								<option defaultValue=\"com.arm.tool.c.compiler.option.optlevel.high\" id=\"com.arm.tool.c.compiler.baremetal.exe.release.base.option.opt.1295874645\" name=\"Optimization level\" superClass=\"com.arm.tool.c.compiler.baremetal.exe.release.base.option.opt\" useByScannerDiscovery=\"true\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.tool.c.compiler.option.targetcpu.567267898\" superClass=\"com.arm.tool.c.compiler.option.targetcpu\" value=\"Cortex-A9\" valueType=\"string\"/>",
		"								<option id=\"com.arm.tool.c.compiler.option.fppcs.675359730\" superClass=\"com.arm.tool.c.compiler.option.fppcs\" value=\"com.arm.tool.c.compiler.option.fppcs.hard\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.tool.c.compiler.option.inst.396505456\" superClass=\"com.arm.tool.c.compiler.option.inst\" value=\"com.arm.tool.c.compiler.option.inst.arm\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.tool.c.compiler.option.incpath.532829948\" superClass=\"com.arm.tool.c.compiler.option.incpath\" valueType=\"includePath\">",
		"									<listOptionValue builtIn=\"false\" value=\"&quot;${workspace_loc:/${ProjName}}&quot;\"/>"
		"									<listOptionValue builtIn=\"false\" value=\"&quot;${workspace_loc:/${ProjName}}/src&quot;\"/>",
		"									<listOptionValue builtIn=\"false\" value=\"&quot;${workspace_loc:/${ProjName}/hwlibs/include}&quot;\"/>",
		"								</option>",
		"								<option id=\"com.arm.tool.c.compiler.option.defmac.1943065363\" superClass=\"com.arm.tool.c.compiler.option.defmac\" valueType=\"definedSymbols\">",
		"									<listOptionValue builtIn=\"false\" value=\"soc_device\"/>",
		"								</option>",
		"								<option id=\"com.arm.tool.c.compile.option.lang.374396639\" superClass=\"com.arm.tool.c.compile.option.lang\" value=\"com.arm.tool.c.compile.option.lang.c99\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.tool.c.compiler.option.strict.1147248911\" name=\"Strict language conformance\" superClass=\"com.arm.tool.c.compiler.option.strict\" useByScannerDiscovery=\"true\" value=\"com.arm.tool.c.compiler.option.strict.error\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.tool.c.compiler.option.warnaserr.1694874995\" name=\"Warnings are errors (--diag_error=warning)\" superClass=\"com.arm.tool.c.compiler.option.warnaserr\" useByScannerDiscovery=\"false\" value=\"true\" valueType=\"boolean\"/>",

		"								<inputType id=\"com.arm.tool.c.compiler.input.1559069133\" superClass=\"com.arm.tool.c.compiler.input\"/>",
		"								<inputType id=\"com.arm.tool.cpp.compiler.input.1062295580\" superClass=\"com.arm.tool.cpp.compiler.input\"/>",
		"							</tool>",
		"							<tool id=\"com.arm.tool.cpp.compiler.baremetal.exe.release.645028646\" name=\"ARM C++ Compiler 5\" superClass=\"com.arm.tool.cpp.compiler.baremetal.exe.release\">",
		"								<option defaultValue=\"com.arm.tool.c.compiler.option.optlevel.high\" id=\"com.arm.tool.cpp.compiler.baremetal.exe.release.base.option.opt.1273918059\" name=\"Optimization level\" superClass=\"com.arm.tool.cpp.compiler.baremetal.exe.release.base.option.opt\" valueType=\"enumerated\"/>",
		"							</tool>",
		"							<tool id=\"com.arm.tool.assembler.78248880\" name=\"ARM Assembler 5\" superClass=\"com.arm.tool.assembler\">",
		"								<option id=\"com.arm.tool.assembler.option.cpu.1168957547\" superClass=\"com.arm.tool.assembler.option.cpu\" value=\"Cortex-A9\" valueType=\"string\"/>",
		"								<option id=\"com.arm.tool.assembler.option.fppcs.1388985764\" superClass=\"com.arm.tool.assembler.option.fppcs\" value=\"com.arm.tool.c.compiler.option.fppcs.hard\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.tool.assembler.option.inst.1390760357\" superClass=\"com.arm.tool.assembler.option.inst\" value=\"com.arm.tool.c.compiler.option.inst.arm\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.tool.assembler.option.err.1432832369\" name=\"Error severity (--diag_error)\" superClass=\"com.arm.tool.assembler.option.err\" value=\"warning\" valueType=\"string\"/>",
		"								<inputType id=\"com.arm.tool.assembler.input.1841940459\" superClass=\"com.arm.tool.assembler.input\"/>",
		"							</tool>",
		"							<tool id=\"com.arm.tool.c.linker.1517549545\" name=\"ARM Linker 5\" superClass=\"com.arm.tool.c.linker\">",
		"								<option id=\"com.arm.tool.c.linker.option.cpu.1240310015\" superClass=\"com.arm.tool.c.linker.option.cpu\" value=\"Cortex-A9\" valueType=\"string\"/>",
		"								<option id=\"com.arm.tool.c.linker.option.scatter.228160240\" superClass=\"com.arm.tool.c.linker.option.scatter\" value=\"${workspace_loc:/${ProjName}}/my_linker_script\" valueType=\"string\"/>",
		"								<option id=\"com.arm.tool.c.linker.option.flags.1209243247\" name=\"Other flags\" superClass=\"com.arm.tool.c.linker.option.flags\" valueType=\"stringList\">",
		"									<listOptionValue builtIn=\"false\" value=\"--entry=alt_interrupt_vector\"/>",
		"								</option>",
		"								<option id=\"com.arm.tool.c.linker.option.err.1427381517\" name=\"Error severity (--diag_error)\" superClass=\"com.arm.tool.c.linker.option.err\" value=\"warning\" valueType=\"string\"/>",
		"							</tool>",
		"							<tool id=\"com.arm.tool.librarian.782458402\" name=\"ARM Librarian 5\" superClass=\"com.arm.tool.librarian\"/>",
		"						</toolChain>",
		"					</folderInfo>",
		"				</configuration>",
		"			</storageModule>",
		"			<storageModule moduleId=\"org.eclipse.cdt.core.externalSettings\"/>",
		"		</cconfiguration>",
		"	</storageModule>",
		"	<storageModule moduleId=\"cdtBuildSystem\" version=\"4.0.0\">",
		"		<project id=\"ArmccHwlibsProject3.com.arm.eclipse.build.project.baremetal.exe.7237795\" name=\"Executable\" projectType=\"com.arm.eclipse.build.project.baremetal.exe\"/>",
		"	</storageModule>",
		"	<storageModule moduleId=\"scannerConfiguration\">",
		"		<autodiscovery enabled=\"true\" problemReportingEnabled=\"true\" selectedProfileId=\"\"/>",
		"	</storageModule>",
		"	<storageModule moduleId=\"org.eclipse.cdt.core.LanguageSettingsProviders\"/>",
		"	<storageModule moduleId=\"com.arm.projectSettings\" version=\"5.25\"/>",
		"</cproject>"
	];

	MAKEFILE_ARMCC_CPROJECT_TEMPLATE = [
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>",
		"<?fileVersion 4.0.0?><cproject storage_type_id=\"org.eclipse.cdt.core.XmlProjectDescriptionStorage\">",
		"	<storageModule moduleId=\"org.eclipse.cdt.core.settings\">",
		"		<cconfiguration id=\"com.arm.toolchain.baremetal.124384934\">",
		"			<storageModule buildSystemId=\"org.eclipse.cdt.managedbuilder.core.configurationDataProvider\" id=\"com.arm.toolchain.baremetal.124384934\" moduleId=\"org.eclipse.cdt.core.settings\" name=\"Default\">",
		"				<externalSettings/>",
		"				<extensions>",
		"					<extension id=\"org.eclipse.cdt.core.GmakeErrorParser\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"com.arm.eclipse.builder.armcc.error\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"org.eclipse.cdt.core.CWDLocator\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"				</extensions>",
		"			</storageModule>",
		"			<storageModule moduleId=\"cdtBuildSystem\" version=\"4.0.0\">",
		"				<configuration buildProperties=\"\" id=\"com.arm.toolchain.baremetal.124384934\" name=\"Default\" parent=\"org.eclipse.cdt.build.core.emptycfg\">",
		"					<folderInfo id=\"com.arm.toolchain.baremetal.124384934.1682968484\" name=\"/\" resourcePath=\"\">",
		"						<toolChain id=\"com.arm.toolchain.baremetal.544700437\" name=\"ARM Compiler 5 (DS-5 built-in)\" superClass=\"com.arm.toolchain.baremetal\">",
		"							<targetPlatform id=\"com.arm.toolchain.baremetal.124384934.872210800\" name=\"\"/>",
		"							<builder id=\"com.arm.toolchain.baremetal.builder.1501025045\" managedBuildOn=\"false\" name=\"Gnu Make Builder.Default\" superClass=\"com.arm.toolchain.baremetal.builder\"/>",
		"							<tool id=\"com.arm.tool.c.compiler.617317026\" name=\"ARM C Compiler 5\" superClass=\"com.arm.tool.c.compiler\"/>",
		"							<tool id=\"com.arm.tool.cpp.compiler.1877459330\" name=\"ARM C++ Compiler 5\" superClass=\"com.arm.tool.cpp.compiler\"/>",
		"							<tool id=\"com.arm.tool.assembler.1294000369\" name=\"ARM Assembler 5\" superClass=\"com.arm.tool.assembler\"/>",
		"							<tool id=\"com.arm.tool.c.linker.177008579\" name=\"ARM Linker 5\" superClass=\"com.arm.tool.c.linker\"/>",
		"							<tool id=\"com.arm.tool.librarian.97482788\" name=\"ARM Librarian 5\" superClass=\"com.arm.tool.librarian\"/>",
		"						</toolChain>",
		"					</folderInfo>",
		"				</configuration>",
		"			</storageModule>",
		"			<storageModule moduleId=\"org.eclipse.cdt.core.externalSettings\"/>",
		"		</cconfiguration>",
		"	</storageModule>",
		"	<storageModule moduleId=\"cdtBuildSystem\" version=\"4.0.0\">",
		"		<project id=\"my_project_name.null.322129056\" name=\"my_project_name\"/>",
		"	</storageModule>",
		"	<storageModule moduleId=\"scannerConfiguration\">",
		"		<autodiscovery enabled=\"true\" problemReportingEnabled=\"true\" selectedProfileId=\"\"/>",
		"	</storageModule>",
		"	<storageModule moduleId=\"org.eclipse.cdt.core.LanguageSettingsProviders\"/>",
		"</cproject>",
	];
	
	PLUGIN_GCC_CPROJECT_TEMPLATE = [
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>",
		"<?fileVersion 4.0.0?><cproject storage_type_id=\"org.eclipse.cdt.core.XmlProjectDescriptionStorage\">",
		"	<storageModule moduleId=\"org.eclipse.cdt.core.settings\">",
		"		<cconfiguration id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.config.exe.debug.var.com.altera.ds5ae.toolchain.gcc.baremetal.1488201438\">",
		"			<storageModule buildSystemId=\"org.eclipse.cdt.managedbuilder.core.configurationDataProvider\" id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.config.exe.debug.var.com.altera.ds5ae.toolchain.gcc.baremetal.1488201438\" moduleId=\"org.eclipse.cdt.core.settings\" name=\"Debug\">",
		"				<externalSettings/>",
		"				<extensions>",
		"					<extension id=\"org.eclipse.cdt.core.GASErrorParser\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"org.eclipse.cdt.core.GmakeErrorParser\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"org.eclipse.cdt.core.GLDErrorParser\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"org.eclipse.cdt.core.CWDLocator\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"org.eclipse.cdt.core.GCCErrorParser\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"				</extensions>",
		"			</storageModule>",
		"			<storageModule moduleId=\"cdtBuildSystem\" version=\"4.0.0\">",
		"				<configuration artifactExtension=\"axf\" artifactName=\"${ProjName}\" buildArtefactType=\"org.eclipse.cdt.build.core.buildArtefactType.exe\" buildProperties=\"org.eclipse.cdt.build.core.buildArtefactType=org.eclipse.cdt.build.core.buildArtefactType.exe,org.eclipse.cdt.build.core.buildType=org.eclipse.cdt.build.core.buildType.debug\" cleanCommand=\"clean\" description=\"\" id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.config.exe.debug.var.com.altera.ds5ae.toolchain.gcc.baremetal.1488201438\" name=\"Debug\" parent=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.config.exe.debug.var.com.altera.ds5ae.toolchain.gcc.baremetal\">",
		"					<folderInfo id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.config.exe.debug.var.com.altera.ds5ae.toolchain.gcc.baremetal.1488201438.\" name=\"/\" resourcePath=\"\">",
		"						<toolChain id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.toolchain.exe.debug.var.com.altera.ds5ae.toolchain.gcc.baremetal.1288331344\" name=\"Altera Baremetal GCC\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.toolchain.exe.debug.var.com.altera.ds5ae.toolchain.gcc.baremetal\">",
		"							<targetPlatform id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.config.exe.debug.var.com.altera.ds5ae.toolchain.gcc.baremetal.1488201438..699145611\" name=\"\"/>",
		"							<builder buildPath=\"${workspace_loc:/GccHwlibsProject}/Debug\" id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.target.builder.1205190822\" keepEnvironmentInBuildfile=\"false\" managedBuildOn=\"true\" name=\"Gnu Make Builder\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.target.builder\"/>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.cpp.compiler.var.com.altera.ds5ae.toolchain.gcc.baremetal.502907787\" name=\"GCC C++ Compiler\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.cpp.compiler.var.com.altera.ds5ae.toolchain.gcc.baremetal\">",
		"								<option defaultValue=\"gnu.c.optimization.level.none\" id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.opt.1732076074\" name=\"Optimization Level\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.opt\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.debug.445788807\" name=\"Debug Level\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.debug\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.arch.453337388\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.arch\" value=\"armv7-a\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.cpu.79368003\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.cpu\" value=\"cortex-a9\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.fpu.237404374\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.fpu\" value=\"neon\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.fabi.1258310991\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.fabi\" value=\"softfp\" valueType=\"string\"/>",
		"							</tool>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.var.com.altera.ds5ae.toolchain.gcc.baremetal.2135409035\" name=\"GCC C Compiler\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.var.com.altera.ds5ae.toolchain.gcc.baremetal\">",
		"								<option defaultValue=\"gnu.c.optimization.level.none\" id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.opt.1364844834\" name=\"Optimization Level\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.opt\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.debug.1116314196\" name=\"Debug Level\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.debug\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.arch.840290374\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.arch\" value=\"armv7-a\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.cpu.1501597053\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.cpu\" value=\"cortex-a9\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.fpu.1456840721\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.fpu\" value=\"neon\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.fabi.633834269\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.fabi\" value=\"softfp\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.other.2102518064\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.other\" value=\"-fdata-sections -ffunction-sections -std=c99\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.includepath.1740042376\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.includepath\" valueType=\"includePath\">",
		"									<listOptionValue builtIn=\"false\" value=\"&quot;${workspace_loc:/${ProjName}}&quot;\"/>",
		"									<listOptionValue builtIn=\"false\" value=\"&quot;${workspace_loc:/${ProjName}}/src&quot;\"/>",
		"									<listOptionValue builtIn=\"false\" value=\"&quot;${workspace_loc:/${ProjName}/hwlibs/include}&quot;\"/>									",
		"									<listOptionValue builtIn=\"false\" value=\"&quot;${workspace_loc:/${ProjName}/hwlibs/include/safeclib}&quot;\"/>									",
		"								</option>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.defsym.2099318398\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.defsym\" valueType=\"definedSymbols\">",
		"									<listOptionValue builtIn=\"false\" value=\"soc_device\"/>",
		"								</option>",
		"<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.warnings.error.973712815\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.warnings.error\" value=\"true\" valueType=\"boolean\"/>",
		"								<inputType id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.input.699156571\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.input\"/>",
		"							</tool>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.var.com.altera.ds5ae.toolchain.gcc.baremetal.1517175549\" name=\"GCC Assembler\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.var.com.altera.ds5ae.toolchain.gcc.baremetal\">",
		"								<option defaultValue=\"true\" id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.debug.1592264239\" name=\"Generate debugging information (-g)\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.debug\" valueType=\"boolean\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.arch.761098925\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.arch\" value=\"armv7-a\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.cpu.1771408373\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.cpu\" value=\"cortex-a9\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.fpu.259477959\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.fpu\" value=\"neon\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.fabi.1018297718\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.fabi\" value=\"softfp\" valueType=\"string\"/>",
		"								<inputType id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.input.1330633158\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.input\"/>",
		"							</tool>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.cpp.linker.var.com.altera.ds5ae.toolchain.gcc.baremetal.582380099\" name=\"GCC C++ Linker\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.cpp.linker.var.com.altera.ds5ae.toolchain.gcc.baremetal\"/>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.var.com.altera.ds5ae.toolchain.gcc.baremetal.264219610\" name=\"GCC C Linker\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.var.com.altera.ds5ae.toolchain.gcc.baremetal\">",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.option.script.114911964\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.option.script\" value=\"${workspace_loc:/${ProjName}}/my_linker_script\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.option.removeunused.1549476786\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.option.removeunused\" value=\"true\" valueType=\"boolean\"/>",
		"<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.option.flags.13018383\" name=\"Other flags\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.option.flags\" value=\"-Wl,--gc-sectionsmy_gcc_enable_semihosting\" valueType=\"string\"/>",
		"								<inputType id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.input.1114667190\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.input\">",
		"									<additionalInput kind=\"additionalinput\" paths=\"$(LIBS)\"/>",
		"									<additionalInput kind=\"additionalinputdependency\" paths=\"$(USER_OBJS)\"/>",
		"								</inputType>",
		"							</tool>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.archiver.var.com.altera.ds5ae.toolchain.gcc.baremetal.899256183\" name=\"GCC Archiver\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.archiver.var.com.altera.ds5ae.toolchain.gcc.baremetal\"/>",
		"						</toolChain>",
		"					</folderInfo>",
		"				</configuration>",
		"			</storageModule>",
		"			<storageModule moduleId=\"org.eclipse.cdt.core.externalSettings\"/>",
		"		</cconfiguration>",
		"		<cconfiguration id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.config.exe.release.var.com.altera.ds5ae.toolchain.gcc.baremetal.189474610\">",
		"			<storageModule buildSystemId=\"org.eclipse.cdt.managedbuilder.core.configurationDataProvider\" id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.config.exe.release.var.com.altera.ds5ae.toolchain.gcc.baremetal.189474610\" moduleId=\"org.eclipse.cdt.core.settings\" name=\"Release\">",
		"				<externalSettings/>",
		"				<extensions>",
		"					<extension id=\"org.eclipse.cdt.core.GASErrorParser\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"org.eclipse.cdt.core.GmakeErrorParser\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"org.eclipse.cdt.core.GLDErrorParser\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"org.eclipse.cdt.core.CWDLocator\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"org.eclipse.cdt.core.GCCErrorParser\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"				</extensions>",
		"			</storageModule>",
		"			<storageModule moduleId=\"cdtBuildSystem\" version=\"4.0.0\">",
		"				<configuration artifactExtension=\"axf\" artifactName=\"${ProjName}\" buildArtefactType=\"org.eclipse.cdt.build.core.buildArtefactType.exe\" buildProperties=\"org.eclipse.cdt.build.core.buildArtefactType=org.eclipse.cdt.build.core.buildArtefactType.exe,org.eclipse.cdt.build.core.buildType=org.eclipse.cdt.build.core.buildType.release\" cleanCommand=\"clean\" description=\"\" id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.config.exe.release.var.com.altera.ds5ae.toolchain.gcc.baremetal.189474610\" name=\"Release\" parent=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.config.exe.release.var.com.altera.ds5ae.toolchain.gcc.baremetal\">",
		"					<folderInfo id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.config.exe.release.var.com.altera.ds5ae.toolchain.gcc.baremetal.189474610.\" name=\"/\" resourcePath=\"\">",
		"						<toolChain id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.toolchain.exe.release.var.com.altera.ds5ae.toolchain.gcc.baremetal.1993271557\" name=\"Altera Baremetal GCC\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.toolchain.exe.release.var.com.altera.ds5ae.toolchain.gcc.baremetal\">",
		"							<targetPlatform id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.config.exe.release.var.com.altera.ds5ae.toolchain.gcc.baremetal.189474610..215257238\" name=\"\"/>",
		"							<builder buildPath=\"${workspace_loc:/GccHwlibsProject}/Release\" id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.target.builder.858991428\" keepEnvironmentInBuildfile=\"false\" managedBuildOn=\"true\" name=\"Gnu Make Builder\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.target.builder\"/>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.cpp.compiler.var.com.altera.ds5ae.toolchain.gcc.baremetal.609390831\" name=\"GCC C++ Compiler\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.cpp.compiler.var.com.altera.ds5ae.toolchain.gcc.baremetal\">",
		"								<option defaultValue=\"gnu.c.optimization.level.most\" id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.opt.1181423040\" name=\"Optimization Level\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.opt\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.debug.77623322\" name=\"Debug Level\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.debug\" value=\"gnu.c.debugging.level.none\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.arch.687586664\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.arch\" value=\"armv7-a\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.cpu.1656910813\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.cpu\" value=\"cortex-a9\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.fpu.1990789317\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.fpu\" value=\"neon\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.fabi.237463182\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.fabi\" value=\"softfp\" valueType=\"string\"/>",
		"							</tool>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.var.com.altera.ds5ae.toolchain.gcc.baremetal.971848725\" name=\"GCC C Compiler\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.var.com.altera.ds5ae.toolchain.gcc.baremetal\">",
		"								<option defaultValue=\"gnu.c.optimization.level.most\" id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.opt.1580992747\" name=\"Optimization Level\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.opt\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.debug.1263077464\" name=\"Debug Level\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.debug\" value=\"gnu.c.debugging.level.none\" valueType=\"enumerated\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.arch.2065783842\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.arch\" value=\"armv7-a\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.cpu.1754524813\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.cpu\" value=\"cortex-a9\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.fpu.508275016\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.fpu\" value=\"neon\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.fabi.2000570017\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.fabi\" value=\"softfp\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.other.1556905894\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.other\" value=\"-fdata-sections -ffunction-sections -std=c99\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.includepath.1953830293\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.includepath\" valueType=\"includePath\">",
		"									<listOptionValue builtIn=\"false\" value=\"&quot;${workspace_loc:/${ProjName}}&quot;\"/>",
		"									<listOptionValue builtIn=\"false\" value=\"&quot;${workspace_loc:/${ProjName}}/src&quot;\"/>",
		"									<listOptionValue builtIn=\"false\" value=\"&quot;${workspace_loc:/${ProjName}/hwlibs/include}&quot;\"/>",
		"								</option>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.defsym.542893224\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.defsym\" valueType=\"definedSymbols\">",
		"									<listOptionValue builtIn=\"false\" value=\"soc_device\"/>									",
		"								</option>",
		"<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.warnings.error.973742816\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.option.warnings.error\" value=\"true\" valueType=\"boolean\"/>",
		"								<inputType id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.input.792371368\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.input\"/>",
		"							</tool>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.var.com.altera.ds5ae.toolchain.gcc.baremetal.124572025\" name=\"GCC Assembler\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.var.com.altera.ds5ae.toolchain.gcc.baremetal\">",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.arch.964813164\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.arch\" value=\"armv7-a\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.cpu.1038144232\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.cpu\" value=\"cortex-a9\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.fpu.370328629\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.fpu\" value=\"neon\" valueType=\"string\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.fabi.1408738990\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.option.fabi\" value=\"softfp\" valueType=\"string\"/>",
		"								<inputType id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.input.498861865\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.input\"/>",
		"							</tool>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.cpp.linker.var.com.altera.ds5ae.toolchain.gcc.baremetal.2105723506\" name=\"GCC C++ Linker\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.cpp.linker.var.com.altera.ds5ae.toolchain.gcc.baremetal\"/>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.var.com.altera.ds5ae.toolchain.gcc.baremetal.423574873\" name=\"GCC C Linker\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.var.com.altera.ds5ae.toolchain.gcc.baremetal\">",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.option.removeunused.661407077\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.option.removeunused\" value=\"true\" valueType=\"boolean\"/>",
		"								<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.option.script.1748624392\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.option.script\" value=\"${workspace_loc:/${ProjName}}/my_linker_script\" valueType=\"string\"/>",
		"<option id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.option.flags.13018383\" name=\"Other flags\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.option.flags\" value=\"-Wl,--gc-sectionsmy_gcc_enable_semihosting\" valueType=\"string\"/>",
		"								<inputType id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.input.339725443\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.input\">",
		"									<additionalInput kind=\"additionalinput\" paths=\"$(LIBS)\"/>",
		"									<additionalInput kind=\"additionalinputdependency\" paths=\"$(USER_OBJS)\"/>",
		"								</inputType>",
		"							</tool>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.archiver.var.com.altera.ds5ae.toolchain.gcc.baremetal.1067556678\" name=\"GCC Archiver\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.archiver.var.com.altera.ds5ae.toolchain.gcc.baremetal\"/>",
		"						</toolChain>",
		"					</folderInfo>",
		"				</configuration>",
		"			</storageModule>",
		"			<storageModule moduleId=\"org.eclipse.cdt.core.externalSettings\"/>",
		"		</cconfiguration>",
		"	</storageModule>",
		"	<storageModule moduleId=\"cdtBuildSystem\" version=\"4.0.0\">",
		"		<project id=\"GccHwlibsProject.com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.target.exe.var.com.altera.ds5ae.toolchain.gcc.baremetal.660098287\" name=\"Executable\" projectType=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.target.exe.var.com.altera.ds5ae.toolchain.gcc.baremetal\"/>",
		"	</storageModule>",
		"	<storageModule moduleId=\"scannerConfiguration\">",
		"		<autodiscovery enabled=\"true\" problemReportingEnabled=\"true\" selectedProfileId=\"\"/>",
		"		<scannerConfigBuildInfo instanceId=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.config.exe.release.var.com.altera.ds5ae.toolchain.gcc.baremetal.189474610;com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.config.exe.release.var.com.altera.ds5ae.toolchain.gcc.baremetal.189474610.;com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.var.com.altera.ds5ae.toolchain.gcc.baremetal.971848725;com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.input.792371368\">",
		"			<autodiscovery enabled=\"true\" problemReportingEnabled=\"true\" selectedProfileId=\"com.arm.managedbuilder.gcc.ARMGCCDiscoveryProfileC\"/>",
		"		</scannerConfigBuildInfo>",
		"	</storageModule>",
		"	<storageModule moduleId=\"org.eclipse.cdt.core.LanguageSettingsProviders\"/>",
		"	<storageModule moduleId=\"com.arm.projectSettings\" version=\"5.25\"/>",
		"</cproject>"
	];

	MAKEFILE_GCC_CPROJECT_TEMPLATE = [
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>",
		"<?fileVersion 4.0.0?><cproject storage_type_id=\"org.eclipse.cdt.core.XmlProjectDescriptionStorage\">",
		"	<storageModule moduleId=\"org.eclipse.cdt.core.settings\">",
		"		<cconfiguration id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.toolchain.var.com.altera.ds5ae.toolchain.gcc.baremetal.824400856\">",
		"			<storageModule buildSystemId=\"org.eclipse.cdt.managedbuilder.core.configurationDataProvider\" id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.toolchain.var.com.altera.ds5ae.toolchain.gcc.baremetal.824400856\" moduleId=\"org.eclipse.cdt.core.settings\" name=\"Default\">",
		"				<externalSettings/>",
		"				<extensions>",
		"					<extension id=\"org.eclipse.cdt.core.GASErrorParser\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"org.eclipse.cdt.core.GmakeErrorParser\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"org.eclipse.cdt.core.CWDLocator\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"					<extension id=\"org.eclipse.cdt.core.GCCErrorParser\" point=\"org.eclipse.cdt.core.ErrorParser\"/>",
		"				</extensions>",
		"			</storageModule>",
		"			<storageModule moduleId=\"cdtBuildSystem\" version=\"4.0.0\">",
		"				<configuration buildProperties=\"\" id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.toolchain.var.com.altera.ds5ae.toolchain.gcc.baremetal.824400856\" name=\"Default\" parent=\"org.eclipse.cdt.build.core.emptycfg\">",
		"					<folderInfo id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.toolchain.var.com.altera.ds5ae.toolchain.gcc.baremetal.824400856.1833252347\" name=\"/\" resourcePath=\"\">",
		"						<toolChain id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.toolchain.var.com.altera.ds5ae.toolchain.gcc.baremetal.1644320831\" name=\"Altera Baremetal GCC\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.toolchain.var.com.altera.ds5ae.toolchain.gcc.baremetal\">",
		"							<targetPlatform id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.toolchain.var.com.altera.ds5ae.toolchain.gcc.baremetal.824400856.1726271729\" name=\"\"/>",
		"							<builder id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.target.builder.50742584\" managedBuildOn=\"false\" name=\"Gnu Make Builder.Default\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.target.builder\"/>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.cpp.compiler.var.com.altera.ds5ae.toolchain.gcc.baremetal.693769757\" name=\"GCC C++ Compiler\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.cpp.compiler.var.com.altera.ds5ae.toolchain.gcc.baremetal\"/>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.var.com.altera.ds5ae.toolchain.gcc.baremetal.836136077\" name=\"GCC C Compiler\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.compiler.var.com.altera.ds5ae.toolchain.gcc.baremetal\"/>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.var.com.altera.ds5ae.toolchain.gcc.baremetal.21830503\" name=\"GCC Assembler\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.assembler.var.com.altera.ds5ae.toolchain.gcc.baremetal\"/>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.cpp.linker.var.com.altera.ds5ae.toolchain.gcc.baremetal.1132058035\" name=\"GCC C++ Linker\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.cpp.linker.var.com.altera.ds5ae.toolchain.gcc.baremetal\"/>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.var.com.altera.ds5ae.toolchain.gcc.baremetal.207547127\" name=\"GCC C Linker\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.c.linker.var.com.altera.ds5ae.toolchain.gcc.baremetal\"/>",
		"							<tool id=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.archiver.var.com.altera.ds5ae.toolchain.gcc.baremetal.216370764\" name=\"GCC Archiver\" superClass=\"com.arm.eclipse.cdt.managedbuild.ds5.gcc.baremetal.tool.archiver.var.com.altera.ds5ae.toolchain.gcc.baremetal\"/>",
		"						</toolChain>",
		"					</folderInfo>",
		"				</configuration>",
		"			</storageModule>",
		"			<storageModule moduleId=\"org.eclipse.cdt.core.externalSettings\"/>",
		"		</cconfiguration>",
		"	</storageModule>",
		"	<storageModule moduleId=\"cdtBuildSystem\" version=\"4.0.0\">",
		"		<project id=\"my_project_name.null.512950166\" name=\"my_project_name\"/>",
		"	</storageModule>",
		"	<storageModule moduleId=\"scannerConfiguration\">",
		"		<autodiscovery enabled=\"true\" problemReportingEnabled=\"true\" selectedProfileId=\"\"/>",
		"	</storageModule>",
		"	<storageModule moduleId=\"org.eclipse.cdt.core.LanguageSettingsProviders\"/>",
		"</cproject>",
	];

	MAKEFILE_GCC_TEMPLATE = [
		"ALT_DEVICE_FAMILY = soc_device",
		"",
		"LINKER_SCRIPT := my_linker_script",
		"",
		"MULTILIBFLAGS := -mcpu=cortex-a9 -mfloat-abi=softfp -mfpu=neon",
		"ASFLAGS :=  -march=armv7-a -mcpu=cortex-a9",
		"CFLAGS  := -g -O1 -Wall -Werror -std=c99 $(MULTILIBFLAGS) -Werror -fdata-sections -ffunction-sections -Dmy_printf_option -D$(ALT_DEVICE_FAMILY) -Ihwlibs/include -Isrc -I./",
		"LDFLAGS := -T$(LINKER_SCRIPT) $(MULTILIBFLAGS) -Wl,--gc-sectionsmy_gcc_enable_semihosting",
		"",
		"CROSS_COMPILE := arm-altera-eabi-",
		"AS := $(CROSS_COMPILE)as",
		"CC := $(CROSS_COMPILE)gcc",
		"LD := $(CROSS_COMPILE)g++",
		"OC := $(CROSS_COMPILE)objcopy",
		"NM := $(CROSS_COMPILE)nm",
		"OD := $(CROSS_COMPILE)objdump",
		"RM := rm -rf",
		"",
		"HWLIBS_SRC := my_hwlibs_src",
		"",
		"APP_SRC := main.c",
		"",
		"ALL_SRC := $(HWLIBS_SRC) $(APP_SRC)",
		"",
		"OBJS := $(patsubst %.c,%.o,$(ALL_SRC))",
		"OBJS := $(patsubst %.s,%.o,$(OBJS)) ",
		"",
		"OBJDIR := obj",
		"OBJS := $(patsubst %.o,$(OBJDIR)/%.o,$(OBJS)) ",
		"",
		"ELF = my_project_name.axf",
		"MAP = my_project_name.map",
		"",
		".PHONY: all",
		"all: $(ELF) $(MAP)",
		"",
		".PHONY: clean",
		"clean: ",
		"	$(RM) $(ELF) $(OBJS) $(MAP)",
		"",
		"VPATH = src/ hwlibs/src/ obj/",
		"",
		"$(OBJDIR)/%.o: %.c Makefile",
		"	$(CC) $(CFLAGS) -c $< -o $@",
		"	",
		"$(OBJDIR)/%.o: %.s Makefile",
		"	$(AS) $(ASFLAGS) -c $< -o $@",
		"",
		"$(ELF): $(OBJS)	$(LINKER_SCRIPT)",
		"	$(LD) $(LDFLAGS) $(OBJS) -o $@",
		"",
		"$(MAP): $(ELF)",
		"	$(NM) $< > $@",
		"",
		"",
	];


	MAKEFILE_ARMCC_TEMPLATE = [
		"ALT_DEVICE_FAMILY = soc_device",
		"",
		"LINKER_SCRIPT := my_linker_script",
		"",
		"MULTILIBFLAGS := -mcpu=cortex-a9 -mfloat-abi=softfp -mfpu=neon",
		"ASFLAGS := --cpu=Cortex-A9 --apcs=/hardfp --arm -g --diag_error=warning ",
		"CFLAGS  := --cpu=Cortex-A9 --apcs=/hardfp --arm -g --diag_error=warning --c99 --strict -O1 -Dmy_printf_option -D$(ALT_DEVICE_FAMILY) -Ihwlibs/include -Isrc -I./",
		"LDFLAGS := --cpu=Cortex-A9 --scatter=$(LINKER_SCRIPT) --info=sizes --diag_error=warning --entry=alt_interrupt_vector",
		"",
		"AS := armasm",
		"CC := armcc",
		"LD := armlink",
		"RM := rm -rf",
		"",
		"HWLIBS_SRC := my_hwlibs_src",
		"",
		"APP_SRC := main.c",
		"",
		"ALL_SRC := $(HWLIBS_SRC) $(APP_SRC)",
		"",
		"OBJS := $(patsubst %.c,%.o,$(ALL_SRC))",
		"OBJS := $(patsubst %.s,%.o,$(OBJS)) ",
		"",
		"OBJDIR := obj",
		"OBJS := $(patsubst %.o,$(OBJDIR)/%.o,$(OBJS)) ",
		"",
		"ELF = my_project_name.axf",
		"MAP = my_project_name.map",
		"",
		".PHONY: all",
		"all: $(ELF)",
		"",
		".PHONY: clean",
		"clean: ",
		"	$(RM) $(ELF) $(OBJS) $(MAP)",
		"",
		"VPATH = src/ hwlibs/src/ obj/",
		"",
		"$(OBJDIR)/%.o: %.c Makefile",
		"	$(CC) $(CFLAGS) -c $< -o $@",
		"	",
		"$(OBJDIR)/%.o: %.s Makefile",
		"	$(AS) $(ASFLAGS) $< -o $@",
		"",
		"$(ELF): $(OBJS)	$(LINKER_SCRIPT)",
		"	$(LD) $(LDFLAGS) $(OBJS) -o $@",
		"",
	];


	CV_DEBUG_CONFIGURATION_LAUNCH_TEMPLATE = [
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>",
		"<launchConfiguration type=\"com.arm.debugger.launcher2\">",
		"<intAttribute key=\"DEBUG_TAB..RESOURCES.COUNT\" value=\"0\"/>",
		"<intAttribute key=\"FILES.CONNECT_TO_GDB_SERVER.RESOURCES.COUNT\" value=\"0\"/>",
		"<intAttribute key=\"FILES.DEBUG_EXISTING_ANDROID.RESOURCES.COUNT\" value=\"0\"/>",
		"<listAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.0.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.COUNT\" value=\"1\"/>",
		"<listAttribute key=\"FILES.DEBUG_RESIDENT_APP\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.0.TYPE\" value=\"APPLICATION_ON_TARGET\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.1.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.1.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.COUNT\" value=\"2\"/>",
		"<listAttribute key=\"FILES.DOWNLOAD_AND_DEBUG\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.0.TYPE\" value=\"TARGET_DOWNLOAD_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.COUNT\" value=\"3\"/>",
		"<listAttribute key=\"FILES.DOWNLOAD_DEBUG\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.0.TYPE\" value=\"TARGET_DOWNLOAD_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.COUNT\" value=\"3\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_DEBUG_ANDROID.RESOURCES.COUNT\" value=\"0\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.VALUE\" value=\"${workspace_loc:my_project_name.axf}\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG.RESOURCES.COUNT\" value=\"my_ice_debug_resources_count\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.COUNT\" value=\"1\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.COUNT\" value=\"1\"/>",
		"<stringAttribute key=\"FILES.SELECTED_DEBUG_OPEATION\" value=\"ICE_DEBUG\"/>",
		"<stringAttribute key=\"HOST_WORKING_DIR\" value=\"${workspace_loc}\"/>",
		"<booleanAttribute key=\"HOST_WORKING_DIR_USE_DEFAULT\" value=\"true\"/>",
		"<booleanAttribute key=\"KEY_COMMANDS_AFTER_CONNECT\" value=\"false\"/>",
		"<stringAttribute key=\"KEY_COMMANDS_AFTER_CONNECT_TEXT\" value=\"\"/>",
		"<booleanAttribute key=\"RSE_USE_HOSTNAME\" value=\"true\"/>",
		"<stringAttribute key=\"TCP_DISABLE_EXTENDED_MODE\" value=\"true\"/>",
		"<booleanAttribute key=\"TCP_KILL_ON_EXIT\" value=\"false\"/>",
		"<booleanAttribute key=\"VFS_ENABLED\" value=\"true\"/>",
		"<stringAttribute key=\"VFS_LOCAL_DIR\" value=\"${workspace_loc}\"/>",
		"<stringAttribute key=\"VFS_REMOTE_MOUNT\" value=\"/writeable\"/>",
		"<stringAttribute key=\"config_db_activity_name\" value=\"Debug Cortex-A9_0\"/>",
		"<stringAttribute key=\"config_db_connection_keys\" value=\"rvi_address target_initialization_script dtsl_config dtsl_tracecapture_option dtsl_config_script eventviewer_tracesource config_file setup TCP_KILL_ON_EXIT TCP_DISABLE_EXTENDED_MODE\"/>",
		"<stringAttribute key=\"config_db_connection_type\" value=\"Bare Metal Debug\"/>",
		"<stringAttribute key=\"config_db_platform_name\" value=\"Altera - Cyclone V SoC (Dual Core)\"/>",
		"<stringAttribute key=\"config_db_project_type\" value=\"Bare Metal Debug\"/>",
		"<stringAttribute key=\"config_db_project_type_id\" value=\"BARE_METAL\"/>",
		"<stringAttribute key=\"config_db_taxonomy_id\" value=\"/platform/altera/cyclonevsoc_dualcore_\"/>",
		"<stringAttribute key=\"config_file\" value=\"CDB://Altera_DEV5XS1.rvc\"/>",
		"<booleanAttribute key=\"connectOnly\" value=\"false\"/>",
		"<stringAttribute key=\"connection_type\" value=\"USB-Blaster\"/>",
		"<stringAttribute key=\"ds5_launch_configuration_version\" value=\"5.25.0\"/>",
		"<stringAttribute key=\"dtsl_config\" value=\"USBBlaster_DtslScript\"/>",
		"<stringAttribute key=\"dtsl_config_script\" value=\"CDB://dtsl_config_script.py\"/>",
		"<stringAttribute key=\"dtsl_options_file\" value=\"default\"/>",
		"<stringAttribute key=\"dtsl_tracecapture_option\" value=\"options.traceBuffer.traceCapture\"/>",
		"<stringAttribute key=\"eventviewer_tracesource\" value=\"STM\"/>",
		"<booleanAttribute key=\"linuxOS\" value=\"false\"/>",
		"<stringAttribute key=\"rddi_type\" value=\"rddi-debug-rvi\"/>",
		"<booleanAttribute key=\"runAfterConnect\" value=\"false\"/>",
		"<stringAttribute key=\"rvi_address\" value=\"\"/>",
		"<listAttribute key=\"setup\">",
		"<listEntry value=\"CDB://../../../Scripts/altera_debug_server.py\"/>",
		"<listEntry value=\"CDB://rddi-dap_cfg-altera.txt rddi-dap_altera\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"stopAtExpression\" value=\"main\"/>",
		"<stringAttribute key=\"target_initialization_script\" value=\"CDB://../../../Scripts/altera_target_check.py\"/>",
		"</launchConfiguration>"
	];

	AV_DEBUG_CONFIGURATION_LAUNCH_TEMPLATE = [
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>",
		"<launchConfiguration type=\"com.arm.debugger.launcher2\">",
		"<intAttribute key=\"DEBUG_TAB..RESOURCES.COUNT\" value=\"0\"/>",
		"<intAttribute key=\"FILES.CONNECT_TO_GDB_SERVER.RESOURCES.COUNT\" value=\"0\"/>",
		"<intAttribute key=\"FILES.DEBUG_EXISTING_ANDROID.RESOURCES.COUNT\" value=\"0\"/>",
		"<listAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.0.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.COUNT\" value=\"1\"/>",
		"<listAttribute key=\"FILES.DEBUG_RESIDENT_APP\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.0.TYPE\" value=\"APPLICATION_ON_TARGET\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.1.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.1.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.COUNT\" value=\"2\"/>",
		"<listAttribute key=\"FILES.DOWNLOAD_AND_DEBUG\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.0.TYPE\" value=\"TARGET_DOWNLOAD_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.COUNT\" value=\"3\"/>",
		"<listAttribute key=\"FILES.DOWNLOAD_DEBUG\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.0.TYPE\" value=\"TARGET_DOWNLOAD_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.COUNT\" value=\"3\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_DEBUG_ANDROID.RESOURCES.COUNT\" value=\"0\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.VALUE\" value=\"${workspace_loc:my_project_name.axf}\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG.RESOURCES.COUNT\" value=\"my_ice_debug_resources_count\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.COUNT\" value=\"1\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.COUNT\" value=\"1\"/>",
		"<stringAttribute key=\"FILES.SELECTED_DEBUG_OPEATION\" value=\"ICE_DEBUG\"/>",
		"<stringAttribute key=\"HOST_WORKING_DIR\" value=\"${workspace_loc}\"/>",
		"<booleanAttribute key=\"HOST_WORKING_DIR_USE_DEFAULT\" value=\"true\"/>",
		"<booleanAttribute key=\"KEY_COMMANDS_AFTER_CONNECT\" value=\"false\"/>",
		"<stringAttribute key=\"KEY_COMMANDS_AFTER_CONNECT_TEXT\" value=\"\"/>",
		"<booleanAttribute key=\"RSE_USE_HOSTNAME\" value=\"true\"/>",
		"<stringAttribute key=\"TCP_DISABLE_EXTENDED_MODE\" value=\"true\"/>",
		"<booleanAttribute key=\"TCP_KILL_ON_EXIT\" value=\"false\"/>",
		"<booleanAttribute key=\"VFS_ENABLED\" value=\"true\"/>",
		"<stringAttribute key=\"VFS_LOCAL_DIR\" value=\"${workspace_loc}\"/>",
		"<stringAttribute key=\"VFS_REMOTE_MOUNT\" value=\"/writeable\"/>",
		"<stringAttribute key=\"config_db_activity_name\" value=\"Debug Cortex-A9_0\"/>",
		"<stringAttribute key=\"config_db_connection_keys\" value=\"rvi_address target_initialization_script dtsl_config dtsl_tracecapture_option dtsl_config_script eventviewer_tracesource config_file setup TCP_KILL_ON_EXIT TCP_DISABLE_EXTENDED_MODE\"/>",
		"<stringAttribute key=\"config_db_connection_type\" value=\"Bare Metal Debug\"/>",
		"<stringAttribute key=\"config_db_platform_name\" value=\"Altera - Arria V SoC\"/>",
		"<stringAttribute key=\"config_db_project_type\" value=\"Bare Metal Debug\"/>",
		"<stringAttribute key=\"config_db_project_type_id\" value=\"BARE_METAL\"/>",
		"<stringAttribute key=\"config_db_taxonomy_id\" value=\"/platform/altera/arriavsoc\"/>",
		"<stringAttribute key=\"config_file\" value=\"CDB://Altera_DEV5XS1.rvc\"/>",
		"<booleanAttribute key=\"connectOnly\" value=\"false\"/>",
		"<stringAttribute key=\"connection_type\" value=\"USB-Blaster\"/>",
		"<stringAttribute key=\"ds5_launch_configuration_version\" value=\"5.25.0\"/>",
		"<stringAttribute key=\"dtsl_config\" value=\"USBBlaster_DtslScript\"/>",
		"<stringAttribute key=\"dtsl_config_script\" value=\"CDB://dtsl_config_script.py\"/>",
		"<stringAttribute key=\"dtsl_options_file\" value=\"default\"/>",
		"<stringAttribute key=\"dtsl_tracecapture_option\" value=\"options.traceBuffer.traceCapture\"/>",
		"<stringAttribute key=\"eventviewer_tracesource\" value=\"STM\"/>",
		"<booleanAttribute key=\"linuxOS\" value=\"false\"/>",
		"<stringAttribute key=\"rddi_type\" value=\"rddi-debug-rvi\"/>",
		"<booleanAttribute key=\"runAfterConnect\" value=\"false\"/>",
		"<stringAttribute key=\"rvi_address\" value=\"\"/>",
		"<listAttribute key=\"setup\">",
		"<listEntry value=\"CDB://../../../Scripts/altera_debug_server.py\"/>",
		"<listEntry value=\"CDB://rddi-dap_cfg-altera.txt rddi-dap_altera\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"stopAtExpression\" value=\"main\"/>",
		"<stringAttribute key=\"target_initialization_script\" value=\"CDB://../../../Scripts/altera_target_check.py\"/>",
		"</launchConfiguration>"
	];
	
	A10_DEBUG_CONFIGURATION_LAUNCH_TEMPLATE = [
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>",
		"<launchConfiguration type=\"com.arm.debugger.launcher2\">",
		"<intAttribute key=\"DEBUG_TAB..RESOURCES.COUNT\" value=\"0\"/>",
		"<intAttribute key=\"FILES.CONNECT_TO_GDB_SERVER.RESOURCES.COUNT\" value=\"0\"/>",
		"<intAttribute key=\"FILES.DEBUG_EXISTING_ANDROID.RESOURCES.COUNT\" value=\"0\"/>",
		"<listAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.0.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.COUNT\" value=\"1\"/>",
		"<listAttribute key=\"FILES.DEBUG_RESIDENT_APP\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.0.TYPE\" value=\"APPLICATION_ON_TARGET\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.1.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.1.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.COUNT\" value=\"2\"/>",
		"<listAttribute key=\"FILES.DOWNLOAD_AND_DEBUG\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.0.TYPE\" value=\"TARGET_DOWNLOAD_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.COUNT\" value=\"3\"/>",
		"<listAttribute key=\"FILES.DOWNLOAD_DEBUG\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.0.TYPE\" value=\"TARGET_DOWNLOAD_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.COUNT\" value=\"3\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_DEBUG_ANDROID.RESOURCES.COUNT\" value=\"0\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.VALUE\" value=\"${workspace_loc:my_project_name.axf}\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG.RESOURCES.COUNT\" value=\"my_ice_debug_resources_count\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.COUNT\" value=\"1\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.COUNT\" value=\"1\"/>",
		"<stringAttribute key=\"FILES.SELECTED_DEBUG_OPEATION\" value=\"ICE_DEBUG\"/>",
		"<stringAttribute key=\"HOST_WORKING_DIR\" value=\"${workspace_loc}\"/>",
		"<booleanAttribute key=\"HOST_WORKING_DIR_USE_DEFAULT\" value=\"true\"/>",
		"<booleanAttribute key=\"KEY_COMMANDS_AFTER_CONNECT\" value=\"false\"/>",
		"<stringAttribute key=\"KEY_COMMANDS_AFTER_CONNECT_TEXT\" value=\"\"/>",
		"<booleanAttribute key=\"RSE_USE_HOSTNAME\" value=\"true\"/>",
		"<stringAttribute key=\"TCP_DISABLE_EXTENDED_MODE\" value=\"true\"/>",
		"<booleanAttribute key=\"TCP_KILL_ON_EXIT\" value=\"false\"/>",
		"<booleanAttribute key=\"VFS_ENABLED\" value=\"true\"/>",
		"<stringAttribute key=\"VFS_LOCAL_DIR\" value=\"${workspace_loc}\"/>",
		"<stringAttribute key=\"VFS_REMOTE_MOUNT\" value=\"/writeable\"/>",
		"<stringAttribute key=\"config_db_activity_name\" value=\"Debug Cortex-A9_0\"/>",
		"<stringAttribute key=\"config_db_connection_keys\" value=\"rvi_address dtsl_config dtsl_tracecapture_option dtsl_config_script eventviewer_tracesource config_file setup TCP_KILL_ON_EXIT TCP_DISABLE_EXTENDED_MODE\"/>",
		"<stringAttribute key=\"config_db_connection_type\" value=\"Bare Metal Debug\"/>",
		"<stringAttribute key=\"config_db_platform_name\" value=\"Altera - Arria 10 SoC\"/>",
		"<stringAttribute key=\"config_db_project_type\" value=\"Bare Metal Debug\"/>",
		"<stringAttribute key=\"config_db_project_type_id\" value=\"BARE_METAL\"/>",
		"<stringAttribute key=\"config_db_taxonomy_id\" value=\"/platform/altera/arria10soc\"/>",
		"<stringAttribute key=\"config_file\" value=\"CDB://Arria10SoC.rvc\"/>",
		"<booleanAttribute key=\"connectOnly\" value=\"false\"/>",
		"<stringAttribute key=\"connection_type\" value=\"USB-Blaster\"/>",
		"<stringAttribute key=\"ds5_launch_configuration_version\" value=\"5.25.0\"/>",
		"<stringAttribute key=\"dtsl_config\" value=\"USBBlaster_DtslScript\"/>",
		"<stringAttribute key=\"dtsl_config_script\" value=\"CDB://dtsl_config_script.py\"/>",
		"<stringAttribute key=\"dtsl_options_file\" value=\"default\"/>",
		"<stringAttribute key=\"dtsl_tracecapture_option\" value=\"options.trace.traceCapture\"/>",
		"<stringAttribute key=\"eventviewer_tracesource\" value=\"STM\"/>",
		"<booleanAttribute key=\"linuxOS\" value=\"false\"/>",
		"<stringAttribute key=\"rddi_type\" value=\"rddi-debug-rvi\"/>",
		"<booleanAttribute key=\"runAfterConnect\" value=\"false\"/>",
		"<stringAttribute key=\"rvi_address\" value=\"\"/>",
		"<listAttribute key=\"setup\">",
		"<listEntry value=\"CDB://../../../Scripts/altera_debug_server.py\"/>",
		"<listEntry value=\"CDB://rddi-dap_cfg-altera.txt rddi-dap_altera\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"stopAtExpression\" value=\"main\"/>",
		"</launchConfiguration>",
	];

	FPGA_CONFIG_LAUNCH_TEMPLATE = [
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>",
		"<launchConfiguration type=\"org.eclipse.ui.externaltools.ProgramLaunchConfigurationType\">",
		"<stringAttribute key=\"org.eclipse.ui.externaltools.ATTR_LOCATION\" value=\"${env_var:SOCEDS_DEST_ROOT}/../qprogrammer/bin/quartus_pgm\"/>",
		"<stringAttribute key=\"org.eclipse.ui.externaltools.ATTR_TOOL_ARGUMENTS\" value=\"--no_banner --mode=jtag -o &quot;p;my_sof_path&quot;\"/>",
		"</launchConfiguration>",
	];

	CREATE_HEADERS_LAUNCH_TEMPLATE = [
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>",
		"<launchConfiguration type=\"org.eclipse.ui.externaltools.ProgramLaunchConfigurationType\">",
		"<stringAttribute key=\"org.eclipse.ui.externaltools.ATTR_LOCATION\" value=\"${env_var:QUARTUS_ROOTDIR}/sopc_builder/bin/sopc-create-header-files\"/>",
		"<stringAttribute key=\"org.eclipse.ui.externaltools.ATTR_TOOL_ARGUMENTS\" value=\"my_sopcinfo --module my_module --single my_system_file\"/>",
		"</launchConfiguration>",
	];

	CV_PRELOADER_LAUNCH_TEMPLATE = [
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>",
		"<launchConfiguration type=\"com.arm.debugger.launcher2\">",
		"<intAttribute key=\"DEBUG_TAB..RESOURCES.COUNT\" value=\"0\"/>",
		"<intAttribute key=\"FILES.CONNECT_TO_GDB_SERVER.RESOURCES.COUNT\" value=\"0\"/>",
		"<intAttribute key=\"FILES.DEBUG_EXISTING_ANDROID.RESOURCES.COUNT\" value=\"0\"/>",
		"<listAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.0.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.COUNT\" value=\"1\"/>",
		"<listAttribute key=\"FILES.DEBUG_RESIDENT_APP\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.0.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.1.TYPE\" value=\"APPLICATION_ON_TARGET\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.1.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.COUNT\" value=\"2\"/>",
		"<listAttribute key=\"FILES.DOWNLOAD_AND_DEBUG\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.0.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.TYPE\" value=\"TARGET_DOWNLOAD_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.COUNT\" value=\"3\"/>",
		"<listAttribute key=\"FILES.DOWNLOAD_DEBUG\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.0.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.TYPE\" value=\"TARGET_DOWNLOAD_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.COUNT\" value=\"3\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_DEBUG_ANDROID.RESOURCES.COUNT\" value=\"0\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG.RESOURCES.COUNT\" value=\"1\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.COUNT\" value=\"1\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.COUNT\" value=\"1\"/>",
		"<stringAttribute key=\"FILES.SELECTED_DEBUG_OPEATION\" value=\"ICE_DEBUG\"/>",
		"<stringAttribute key=\"HOST_WORKING_DIR\" value=\"${workspace_loc}\"/>",
		"<booleanAttribute key=\"HOST_WORKING_DIR_USE_DEFAULT\" value=\"true\"/>",
		"<booleanAttribute key=\"KEY_COMMANDS_AFTER_CONNECT\" value=\"true\"/>",
		"<stringAttribute key=\"KEY_COMMANDS_AFTER_CONNECT_TEXT\" value=\"quit\"/>",
		"<booleanAttribute key=\"RSE_USE_HOSTNAME\" value=\"true\"/>",
		"<stringAttribute key=\"TCP_DISABLE_EXTENDED_MODE\" value=\"true\"/>",
		"<booleanAttribute key=\"TCP_KILL_ON_EXIT\" value=\"false\"/>",
		"<booleanAttribute key=\"VFS_ENABLED\" value=\"true\"/>",
		"<stringAttribute key=\"VFS_LOCAL_DIR\" value=\"${workspace_loc}\"/>",
		"<stringAttribute key=\"VFS_REMOTE_MOUNT\" value=\"/writeable\"/>",
		"<stringAttribute key=\"config_db_activity_name\" value=\"Debug Cortex-A9_0\"/>",
		"<stringAttribute key=\"config_db_connection_keys\" value=\"rvi_address target_initialization_script dtsl_config dtsl_tracecapture_option dtsl_config_script eventviewer_tracesource config_file setup TCP_KILL_ON_EXIT TCP_DISABLE_EXTENDED_MODE\"/>",
		"<stringAttribute key=\"config_db_connection_type\" value=\"Bare Metal Debug\"/>",
		"<stringAttribute key=\"config_db_platform_name\" value=\"Altera - Cyclone V SoC (Dual Core)\"/>",
		"<stringAttribute key=\"config_db_project_type\" value=\"Bare Metal Debug\"/>",
		"<stringAttribute key=\"config_db_project_type_id\" value=\"BARE_METAL\"/>",
		"<stringAttribute key=\"config_db_taxonomy_id\" value=\"/platform/altera/cyclonevsoc_dualcore_\"/>",
		"<stringAttribute key=\"config_file\" value=\"CDB://Altera_DEV5XS1.rvc\"/>",
		"<booleanAttribute key=\"connectOnly\" value=\"true\"/>",
		"<stringAttribute key=\"connection_type\" value=\"USB-Blaster\"/>",
		"<stringAttribute key=\"ds5_launch_configuration_version\" value=\"5.25.0\"/>",
		"<stringAttribute key=\"dtsl_config\" value=\"USBBlaster_DtslScript\"/>",
		"<stringAttribute key=\"dtsl_config_script\" value=\"CDB://dtsl_config_script.py\"/>",
		"<stringAttribute key=\"dtsl_options_file\" value=\"default\"/>",
		"<stringAttribute key=\"dtsl_tracecapture_option\" value=\"options.traceBuffer.traceCapture\"/>",
		"<stringAttribute key=\"eventviewer_tracesource\" value=\"STM\"/>",
		"<booleanAttribute key=\"linuxOS\" value=\"false\"/>",
		"<stringAttribute key=\"rddi_type\" value=\"rddi-debug-rvi\"/>",
		"<booleanAttribute key=\"runAfterConnect\" value=\"false\"/>",
		"<stringAttribute key=\"runScriptAfterConnect\" value=\"my_bootloader_script\"/>",
		"<stringAttribute key=\"rvi_address\" value=\"\"/>",
		"<listAttribute key=\"setup\">",
		"<listEntry value=\"CDB://../../../Scripts/altera_debug_server.py\"/>",
		"<listEntry value=\"CDB://rddi-dap_cfg-altera.txt rddi-dap_altera\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"target_initialization_script\" value=\"CDB://../../../Scripts/altera_target_check.py\"/>",
		"</launchConfiguration>"
	];

	AV_PRELOADER_LAUNCH_TEMPLATE = [
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>",
		"<launchConfiguration type=\"com.arm.debugger.launcher2\">",
		"<intAttribute key=\"DEBUG_TAB..RESOURCES.COUNT\" value=\"0\"/>",
		"<intAttribute key=\"FILES.CONNECT_TO_GDB_SERVER.RESOURCES.COUNT\" value=\"0\"/>",
		"<intAttribute key=\"FILES.DEBUG_EXISTING_ANDROID.RESOURCES.COUNT\" value=\"0\"/>",
		"<listAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.0.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.COUNT\" value=\"1\"/>",
		"<listAttribute key=\"FILES.DEBUG_RESIDENT_APP\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.0.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.1.TYPE\" value=\"APPLICATION_ON_TARGET\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.1.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.COUNT\" value=\"2\"/>",
		"<listAttribute key=\"FILES.DOWNLOAD_AND_DEBUG\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.0.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.TYPE\" value=\"TARGET_DOWNLOAD_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.COUNT\" value=\"3\"/>",
		"<listAttribute key=\"FILES.DOWNLOAD_DEBUG\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.0.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.TYPE\" value=\"TARGET_DOWNLOAD_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.COUNT\" value=\"3\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_DEBUG_ANDROID.RESOURCES.COUNT\" value=\"0\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG.RESOURCES.COUNT\" value=\"1\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.COUNT\" value=\"1\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.COUNT\" value=\"1\"/>",
		"<stringAttribute key=\"FILES.SELECTED_DEBUG_OPEATION\" value=\"ICE_DEBUG\"/>",
		"<stringAttribute key=\"HOST_WORKING_DIR\" value=\"${workspace_loc}\"/>",
		"<booleanAttribute key=\"HOST_WORKING_DIR_USE_DEFAULT\" value=\"true\"/>",
		"<booleanAttribute key=\"KEY_COMMANDS_AFTER_CONNECT\" value=\"true\"/>",
		"<stringAttribute key=\"KEY_COMMANDS_AFTER_CONNECT_TEXT\" value=\"quit\"/>",
		"<booleanAttribute key=\"RSE_USE_HOSTNAME\" value=\"true\"/>",
		"<stringAttribute key=\"TCP_DISABLE_EXTENDED_MODE\" value=\"true\"/>",
		"<booleanAttribute key=\"TCP_KILL_ON_EXIT\" value=\"false\"/>",
		"<booleanAttribute key=\"VFS_ENABLED\" value=\"true\"/>",
		"<stringAttribute key=\"VFS_LOCAL_DIR\" value=\"${workspace_loc}\"/>",
		"<stringAttribute key=\"VFS_REMOTE_MOUNT\" value=\"/writeable\"/>",
		"<stringAttribute key=\"config_db_activity_name\" value=\"Debug Cortex-A9_0\"/>",
		"<stringAttribute key=\"config_db_connection_keys\" value=\"rvi_address target_initialization_script dtsl_config dtsl_tracecapture_option dtsl_config_script eventviewer_tracesource config_file setup TCP_KILL_ON_EXIT TCP_DISABLE_EXTENDED_MODE\"/>",
		"<stringAttribute key=\"config_db_connection_type\" value=\"Bare Metal Debug\"/>",
		"<stringAttribute key=\"config_db_platform_name\" value=\"Altera - Arria V SoC\"/>",
		"<stringAttribute key=\"config_db_project_type\" value=\"Bare Metal Debug\"/>",
		"<stringAttribute key=\"config_db_project_type_id\" value=\"BARE_METAL\"/>",
		"<stringAttribute key=\"config_db_taxonomy_id\" value=\"/platform/altera/arriavsoc\"/>",
		"<stringAttribute key=\"config_file\" value=\"CDB://Altera_DEV5XS1.rvc\"/>",
		"<booleanAttribute key=\"connectOnly\" value=\"true\"/>",
		"<stringAttribute key=\"connection_type\" value=\"USB-Blaster\"/>",
		"<stringAttribute key=\"ds5_launch_configuration_version\" value=\"5.25.0\"/>",
		"<stringAttribute key=\"dtsl_config\" value=\"USBBlaster_DtslScript\"/>",
		"<stringAttribute key=\"dtsl_config_script\" value=\"CDB://dtsl_config_script.py\"/>",
		"<stringAttribute key=\"dtsl_options_file\" value=\"default\"/>",
		"<stringAttribute key=\"dtsl_tracecapture_option\" value=\"options.traceBuffer.traceCapture\"/>",
		"<stringAttribute key=\"eventviewer_tracesource\" value=\"STM\"/>",
		"<booleanAttribute key=\"linuxOS\" value=\"false\"/>",
		"<stringAttribute key=\"rddi_type\" value=\"rddi-debug-rvi\"/>",
		"<booleanAttribute key=\"runAfterConnect\" value=\"false\"/>",
		"<stringAttribute key=\"runScriptAfterConnect\" value=\"my_bootloader_script\"/>",
		"<stringAttribute key=\"rvi_address\" value=\"\"/>",
		"<listAttribute key=\"setup\">",
		"<listEntry value=\"CDB://../../../Scripts/altera_debug_server.py\"/>",
		"<listEntry value=\"CDB://rddi-dap_cfg-altera.txt rddi-dap_altera\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"target_initialization_script\" value=\"CDB://../../../Scripts/altera_target_check.py\"/>",
		"</launchConfiguration>",
	];

	A10_UBOOT_LAUNCH_TEMPLATE = [
		"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>",
		"<launchConfiguration type=\"com.arm.debugger.launcher2\">",
		"<intAttribute key=\"DEBUG_TAB..RESOURCES.COUNT\" value=\"0\"/>",
		"<intAttribute key=\"FILES.CONNECT_TO_GDB_SERVER.RESOURCES.COUNT\" value=\"0\"/>",
		"<intAttribute key=\"FILES.DEBUG_EXISTING_ANDROID.RESOURCES.COUNT\" value=\"0\"/>",
		"<listAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.0.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DEBUG_RESIDENT_ANDROID.RESOURCES.COUNT\" value=\"1\"/>",
		"<listAttribute key=\"FILES.DEBUG_RESIDENT_APP\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.0.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.1.TYPE\" value=\"APPLICATION_ON_TARGET\"/>",
		"<stringAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.1.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DEBUG_RESIDENT_APP.RESOURCES.COUNT\" value=\"2\"/>",
		"<listAttribute key=\"FILES.DOWNLOAD_AND_DEBUG\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.0.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.TYPE\" value=\"TARGET_DOWNLOAD_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.1.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.2.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_AND_DEBUG.RESOURCES.COUNT\" value=\"3\"/>",
		"<listAttribute key=\"FILES.DOWNLOAD_DEBUG\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.0.TYPE\" value=\"TARGET_WORKING_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.0.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.TYPE\" value=\"TARGET_DOWNLOAD_DIR\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.1.VALUE\" value=\"\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.2.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_DEBUG.RESOURCES.COUNT\" value=\"3\"/>",
		"<intAttribute key=\"FILES.DOWNLOAD_DEBUG_ANDROID.RESOURCES.COUNT\" value=\"0\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG.RESOURCES.COUNT\" value=\"1\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG_WITH_ETB_TRACE.RESOURCES.COUNT\" value=\"1\"/>",
		"<listAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE\">",
		"<listEntry value=\"ON_DEMAND_LOAD\"/>",
		"<listEntry value=\"ALSO_LOAD_SYMBOLS\"/>",
		"</listAttribute>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.OPTION.ALSO_LOAD_SYMBOLS\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.OPTION.ON_DEMAND_LOAD\" value=\"true\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.TYPE\" value=\"APP_ON_HOST_TO_DOWNLOAD\"/>",
		"<stringAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.0.VALUE\" value=\"\"/>",
		"<intAttribute key=\"FILES.ICE_DEBUG_WITH_TRACE.RESOURCES.COUNT\" value=\"1\"/>",
		"<stringAttribute key=\"FILES.SELECTED_DEBUG_OPEATION\" value=\"ICE_DEBUG\"/>",
		"<stringAttribute key=\"HOST_WORKING_DIR\" value=\"${workspace_loc}\"/>",
		"<booleanAttribute key=\"HOST_WORKING_DIR_USE_DEFAULT\" value=\"true\"/>",
		"<booleanAttribute key=\"KEY_COMMANDS_AFTER_CONNECT\" value=\"true\"/>",
		"<stringAttribute key=\"KEY_COMMANDS_AFTER_CONNECT_TEXT\" value=\"quit\"/>",
		"<booleanAttribute key=\"RSE_USE_HOSTNAME\" value=\"true\"/>",
		"<stringAttribute key=\"TCP_DISABLE_EXTENDED_MODE\" value=\"true\"/>",
		"<booleanAttribute key=\"TCP_KILL_ON_EXIT\" value=\"false\"/>",
		"<booleanAttribute key=\"VFS_ENABLED\" value=\"true\"/>",
		"<stringAttribute key=\"VFS_LOCAL_DIR\" value=\"${workspace_loc}\"/>",
		"<stringAttribute key=\"VFS_REMOTE_MOUNT\" value=\"/writeable\"/>",
		"<stringAttribute key=\"config_db_activity_name\" value=\"Debug Cortex-A9_0\"/>",
		"<stringAttribute key=\"config_db_connection_keys\" value=\"rvi_address dtsl_config dtsl_tracecapture_option dtsl_config_script eventviewer_tracesource config_file setup TCP_KILL_ON_EXIT TCP_DISABLE_EXTENDED_MODE\"/>",
		"<stringAttribute key=\"config_db_connection_type\" value=\"Bare Metal Debug\"/>",
		"<stringAttribute key=\"config_db_platform_name\" value=\"Altera - Arria 10 SoC\"/>",
		"<stringAttribute key=\"config_db_project_type\" value=\"Bare Metal Debug\"/>",
		"<stringAttribute key=\"config_db_project_type_id\" value=\"BARE_METAL\"/>",
		"<stringAttribute key=\"config_db_taxonomy_id\" value=\"/platform/altera/arria10soc\"/>",
		"<stringAttribute key=\"config_file\" value=\"CDB://Arria10SoC.rvc\"/>",
		"<booleanAttribute key=\"connectOnly\" value=\"true\"/>",
		"<stringAttribute key=\"connection_type\" value=\"USB-Blaster\"/>",
		"<stringAttribute key=\"ds5_launch_configuration_version\" value=\"5.25.0\"/>",
		"<stringAttribute key=\"dtsl_config\" value=\"USBBlaster_DtslScript\"/>",
		"<stringAttribute key=\"dtsl_config_script\" value=\"CDB://dtsl_config_script.py\"/>",
		"<stringAttribute key=\"dtsl_options_file\" value=\"default\"/>",
		"<stringAttribute key=\"dtsl_tracecapture_option\" value=\"options.trace.traceCapture\"/>",
		"<stringAttribute key=\"eventviewer_tracesource\" value=\"STM\"/>",
		"<booleanAttribute key=\"linuxOS\" value=\"false\"/>",
		"<stringAttribute key=\"rddi_type\" value=\"rddi-debug-rvi\"/>",
		"<booleanAttribute key=\"runAfterConnect\" value=\"false\"/>",
		"<stringAttribute key=\"runScriptAfterConnect\" value=\"my_bootloader_script\"/>",
		"<stringAttribute key=\"rvi_address\" value=\"\"/>",
		"<listAttribute key=\"setup\">",
		"<listEntry value=\"CDB://../../../Scripts/altera_debug_server.py\"/>",
		"<listEntry value=\"CDB://rddi-dap_cfg-altera.txt rddi-dap_altera\"/>",
		"</listAttribute>",
		"</launchConfiguration>",
	];

def main():
	global SOCEDS_ROOT;
	global DEVICE;
	global COMPILER;
	global PRINTF_METHOD;
	global PROJECT_NAME;
	global PROJECT_TYPE;
	global MEMORY;
	global BOOTLOADER_SCRIPT;
	global FPGA_CONFIG_FILE;
	global SVD_FOLDER;
	global SOPCINFO_FILE;

	# initialize termplates
	initialize_templates();
	
	# check if we are run from Embedded Command Shell
	SOCEDS_ROOT = os.getenv("SOCEDS_DEST_ROOT", "");
	if SOCEDS_ROOT  == "":
		print "ERROR: SOCEDS_DEST_ROOT variable not defined. Please script run from embedded command shell."
		exit()
	if not  os.path.isdir(SOCEDS_ROOT):
		print "ERROR: SOCEDS_DEST_ROOT path not correct. Please script run from embedded command shell."
		exit()


	# read version
	with open(SOCEDS_ROOT + "/version.txt") as verfile:
		version = verfile.read().replace("\n", " ");

	# display warning if necessary
	if not "Version: 16.1, Build: 196" in version:
		print_error("Warning: Tested with SoC EDS 16.1b196. Not guaranteed to work with other versions.");


	# usage string, for help]
	USAGE = "Usage: " + sys.argv[0] + " \\\n    --project-name=<project_name> \\\n    [--project-type=<plugin|makefile>] \\\n    --device=<av|cv|a10> \\\n    --compiler=<armcc|gcc> \\\n    --printf-method=<uart|semihosting> \\\n    --memory=<ocram|sdram> \\\n    [--bootloader-script=<default|file_name>] \\\n    [--fpga-config-file=<default|file_name>] \\\n    [--svd-folder=<default|folder_name>] \\\n    [--sopcinfo-file=<default|file_name>]";
	
	# parse input arguments
	try:
		opts, args = getopt.getopt(sys.argv[1:],"",["project-name=","project-type=","device=","compiler=","printf-method=","memory=","bootloader-script=","fpga-config-file=","svd-folder=","sopcinfo-file=","help"])
	except getopt.GetoptError:
		print USAGE;
		exit()		
	for opt, arg in opts:
		if opt in ("--project-name"):
			PROJECT_NAME = arg
		elif opt in ("--project-type"):
			PROJECT_TYPE = arg				
		elif opt in ("--device"):
			DEVICE = arg.lower()		
		elif opt in ("--compiler"):
			COMPILER = arg.lower()
		elif opt in ("--printf-method"):
			PRINTF_METHOD = arg.lower()
		elif opt in ("--memory"):
			MEMORY = arg.lower()
		elif opt in ("--bootloader-script"):
			BOOTLOADER_SCRIPT = arg;
		elif opt in ("--fpga-config-file"):
			FPGA_CONFIG_FILE = arg;
		elif opt in ("--svd-folder"):
			SVD_FOLDER = arg;
		elif opt in ("--sopcinfo-file"):
			SOPCINFO_FILE = arg;
		elif opt in ("--help"):
			print USAGE;
			exit()
		else:
			print_error("Error: Unknown option " + opt + "=" + arg);
			print USAGE;
			exit()

	# check input arguments
	if PROJECT_NAME == "":
		print_error("Errror: 'project-name' is empty!");
		print USAGE;
		exit()

	if (PROJECT_TYPE <> "plugin") and (PROJECT_TYPE <> "makefile"):
		print_error("Errror: 'project-type' must be either 'plugin' or 'makefile'!");
		print USAGE;
		exit()

	if (DEVICE <> "cv") and (DEVICE <> "av") and (DEVICE <> "a10"):
		print_error("Errror: device is not one of <cv/av/a10>!");
		print USAGE;
		exit()
		
	if (COMPILER <> "armcc") and (COMPILER <> "gcc"):
		print_error("Errror: compiler is not one of <gcc/armcc>!");
		print USAGE;
		exit()	
		
	if (PRINTF_METHOD <> "uart") and (PRINTF_METHOD <> "semihosting"):
		print_error("Errror: printf-method is not one of <uart/semihosting>!");
		print USAGE;
		exit()	

	if (MEMORY <> "ocram") and (MEMORY <> "sdram"):
		print_error("Errror: memory is not one of <ocram/sdram>!");
		print USAGE;
		exit()

	if BOOTLOADER_SCRIPT <> "":
		if BOOTLOADER_SCRIPT <> "default":
			if not os.path.isfile(BOOTLOADER_SCRIPT):
				print_error("Errror: bootloader-script " + BOOTLOADER_SCRIPT + " was not found!");
				exit()

	if FPGA_CONFIG_FILE  <> "":
		if FPGA_CONFIG_FILE <> "default":
			if not os.path.isfile(BOOTLOADER_SCRIPT):
				print_error("Errror: fpga-config-file " + FPGA_CONFIG_FILE + " was not found!");
				exit()

	if SVD_FOLDER  <> "":
		if SVD_FOLDER <> "default":
			if not os.path.isdir(SVD_FOLDER):
				print_error("Errror: svd-folder " + SVD_FOLDER + " was not found!");
				exit()

	if SOPCINFO_FILE <> "":
		if SOPCINFO_FILE <> "default":
			if not os.path.isfile(SOPCINFO_FILE):
				print_error("Errror: sopcinfo-file " + SOPCINFO_FILE + " was not found!");
				exit()
		if "" == os.getenv("QUARTUS_ROOTDIR",""):
			print_error("Error: QUARTUS_ROOTDIR variable not defined. You need to have Quartus installed for generating system.h file!");
			exit()
				
	run_command("rm -rf " + PROJECT_NAME);
	run_command("mkdir -p  " + PROJECT_NAME);
	
	create_project_file(PROJECT_NAME + "/.project");	
	create_cproject_file(PROJECT_NAME + "/.cproject");

	bring_project_files();
	
	run_command("mkdir -p  " + PROJECT_NAME + "/src");
	create_main_c_file( PROJECT_NAME + "/src/main.c");
	create_debug_launcher_file(PROJECT_NAME + "/" + PROJECT_NAME + ".launch");

	if BOOTLOADER_SCRIPT <> "":
		create_bootloader_launcher();

	if FPGA_CONFIG_FILE  <> "":
		create_fpga_config_launcher();

	if SOPCINFO_FILE <> "":
		create_system_h_file();

	if (PROJECT_TYPE == "makefile"):
		create_makefile();
		run_command("mkdir -p  " + PROJECT_NAME + "/obj");
	
if __name__ == "__main__":
    main()
